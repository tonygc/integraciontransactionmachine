﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.InputRequests
{
    public class LoginRequest
    {
        public string usr { get; set; }
        public string pwd { get; set; }
    }
}
