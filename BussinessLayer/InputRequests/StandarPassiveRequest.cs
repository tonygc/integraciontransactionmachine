﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.InputRequests
{
    public class StandarPassiveRequest
    {
        public int PROID { get; set; }
        public string PRONAME { get; set; }
        public decimal AMOUNT { get; set; }
        public DateTime DATE { get; set; }
        public long[] IDS { get; set; }
    }
}
