﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.InputRequests
{
    public class paymentEmailRequest
    {
        
        public bool sendEmail { get; set; }
        public IEnumerable<paymentDetail> Payments { get; set; }
        public paymentEmailRequest()
        {
            //PRO_CODIGO = string.Empty;
            //PRO_NOMBRE = string.Empty;
            Payments = new List<paymentDetail>();
        }
    }
    public class paymentDetail
    {
        public string ARCHIVO_DE_ORIGEN_DE_TRANSACCI { get; set; }
        public string ESTADO_DE_COINCIDENCIA { get; set; }
        public string COINCIDENCIA_EN { get; set; }
        public string ING_FECHA { get; set; }
        public string PRO_CODIGO { get; set; }
        public string PRO_NOMBRE { get; set; }
        public string CI_CODIGO { get; set; }
        public string CRI_DESCRIPCION { get; set; }
        public string FOLTDA_CODIGO { get; set; }
        public string FOLIOCC { get; set; }
        public decimal ING_SINCONTRIBUCION { get; set; }
        public string ING_IMPORTE { get; set; }
        public string ING_CARGO { get; set; }
        public string ING_CONTRIBUCION { get; set; }
        public string ING_REFERENCIA { get; set; }
        public string DIS_CONTABLE { get; set; }
        public string CENTROCOSTO { get; set; }
        public string REGIONCONTABLE { get; set; }
        public string ZONACONTABLE { get; set; }
        //    public string ING_FECHA { get; set; }
        //    public string CRI_DESCRIPCION { get; set; }
        //    public string FOLIOCC { get; set; }
        //    public string ING_SINCONTRIBUCION { get; set; }
        //    public string ING_REFERENCIA { get; set; }
    }
}
