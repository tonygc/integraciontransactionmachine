﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.InputRequests
{
    public class InvoiceAmount
    {
        public string currencyCode { get; set; }
        public double Amount { get; set; }
    }

    public class _Amount
    {
        public string currencyCode { get; set; }
        public double Amount { get; set; }
    }

    public class InvoiceInterfaceLine
    {
        public string AccountingDate { get; set; }
        public _Amount Amount { get; set; }
        public string AwtGroupName { get; set; }
        public string DistCodeConcatenated { get; set; }
        public int LineNumber { get; set; }
        public string TaxClassificationCode { get; set; }
        public string BudgetDate { get; set; }
        public string Description { get; set; }
    }

    public class InvoiceInterfaceHeader
    {
        public string OperatingUnit { get; set; }
        public string VendorName { get; set; }
        public int VendorNumber { get; set; }
        public string VendorSiteCode { get; set; }
        public string Description { get; set; }
        public string GlDate { get; set; }
        public string GlobalAttribute1 { get; set; }
        public string GroupId { get; set; }
        public string AttributeNumber1 { get; set; }
        public InvoiceAmount InvoiceAmount { get; set; }
        public string InvoiceCurrencyCode { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceNumber { get; set; }
        public string PrepayNumber { get; set; }
        public string InvoiceTypeLookupCode { get; set; }
        public List<InvoiceInterfaceLine> InvoiceInterfaceLine { get; set; }
        public InvoiceInterfaceHeader()
        {
            InvoiceInterfaceLine = new List<InvoiceInterfaceLine>();
        }
    }
    public class InvoiceInterfaceHeaderRequest
    {
        public InvoiceInterfaceHeader invoiceInterfaceHeader { get; set; }
    }
}
