﻿using BussinessLayer.Interfaces.Definition;
using BussinessLayer.OutputResponses;
using DAL.DataContext;
using DAL.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace BussinessLayer.Interfaces.Implementation
{
    public class TBL_PagosIMP : ITBL_Pagos
    {
        public async Task<BigintResponse> save(TBL_Pagos o)
        {
            BigintResponse resp = new BigintResponse();
            try
            {
                using (TransactionMachineCtx ctx = new TransactionMachineCtx())
                {
                    ctx.TBL_Pagos.Add(o);
                    await ctx.SaveChangesAsync();
                    resp.IdGenerated = o.IdPago;
                }
            }
            catch (Exception ex)
            {
                resp.Response = TypeResponse.ERROR;
                string message = ((ex.InnerException as UpdateException)?.InnerException as SqlException)?.Message;
                resp.Message = string.IsNullOrEmpty(message) ? ex.Message : message;
                resp.StackTrace = ex.StackTrace;
            }
            return resp;
        }
        public async Task<TBL_PagosResponse> get(DateTime f1, DateTime f2)
        {
            TBL_PagosResponse resp = new TBL_PagosResponse();
            try
            {
                using (TransactionMachineCtx ctx = new TransactionMachineCtx())
                {
                    f2 = f2.AddDays(1);
                    resp.Data = ctx.TBL_Pagos.Where(a => a.COINCIDENCIA_EN >= f1 && a.COINCIDENCIA_EN < f2).ToList();
                }
            }
            catch (Exception ex)
            {
                resp.Response = TypeResponse.ERROR;
                string message = ((ex.InnerException as UpdateException)?.InnerException as SqlException)?.Message;
                resp.Message = string.IsNullOrEmpty(message) ? ex.Message : message;
                resp.StackTrace = ex.StackTrace;
            }
            return resp;
        }

        public async Task<TBL_PagosResponse> getById(long id)
        {
            TBL_PagosResponse resp = new TBL_PagosResponse();
            try
            {
                using (TransactionMachineCtx ctx = new TransactionMachineCtx())
                {
                    resp.Data = new List<TBL_Pagos> { ctx.TBL_Pagos.FirstOrDefault(a => a.IdPago == id) };
                }
            }
            catch (Exception ex)
            {
                resp.Response = TypeResponse.ERROR;
                string message = ((ex.InnerException as UpdateException)?.InnerException as SqlException)?.Message;
                resp.Message = string.IsNullOrEmpty(message) ? ex.Message : message;
                resp.StackTrace = ex.StackTrace;
            }
            return resp;
        }
        public async Task<GenericResponse> bulkInsert(IEnumerable<TBL_Pagos> o)
        {
            GenericResponse resp = new GenericResponse();
            try
            {
                using (TransactionMachineCtx ctx = new TransactionMachineCtx())
                {
                    //remover de operacion bulk los registros que ya fueron previamente insertados
                    //var bulkClean = new List<TBL_Pagos>();
                    //foreach (var obj in o)
                    //{
                    //    //if (ctx.TBL_Pagos.FirstOrDefault(a => a.FOLIOCC == obj.FOLIOCC) == null)
                    //    //bulkClean.Add(obj);
                    //}
                    ctx.BulkInsert(o, options =>
                    {
                        options.InsertIfNotExists = true;
                        options.ColumnPrimaryKeyExpression = c => c.FOLIOCC;
                    });
                    await ctx.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                resp.Response = TypeResponse.ERROR;
                string message = ((ex.InnerException as UpdateException)?.InnerException as SqlException)?.Message;
                resp.Message = string.IsNullOrEmpty(message) ? ex.Message : message;
                resp.StackTrace = ex.StackTrace;
            }
            return resp;
        }

        public async Task<GenericResponse> bulkUpdate(IEnumerable<TBL_Pagos> o)
        {
            GenericResponse resp = new GenericResponse();
            try
            {
                using (TransactionMachineCtx ctx = new TransactionMachineCtx())
                {
                    //remover de operacion bulk los registros que ya fueron previamente insertados
                    //var bulkClean = new List<TBL_Pagos>();
                    //foreach (var obj in o)
                    //{
                    //    //if (ctx.TBL_Pagos.FirstOrDefault(a => a.FOLIOCC == obj.FOLIOCC) == null)
                    //    //bulkClean.Add(obj);
                    //}
                    ctx.BulkUpdate(o);
                    await ctx.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                resp.Response = TypeResponse.ERROR;
                string message = ((ex.InnerException as UpdateException)?.InnerException as SqlException)?.Message;
                resp.Message = string.IsNullOrEmpty(message) ? ex.Message : message;
                resp.StackTrace = ex.StackTrace;
            }
            return resp;
        }

        public async Task<GenericResponse> update(TBL_Pagos o)
        {
            GenericResponse resp = new GenericResponse();
            try
            {
                using (TransactionMachineCtx ctx = new TransactionMachineCtx())
                {
                    var objToUpdate = ctx.TBL_Pagos.FirstOrDefault(a => a.IdPago == o.IdPago);
                    objToUpdate.InvoiceId = o.InvoiceId;
                    objToUpdate.InvoiceNumber = o.InvoiceNumber;
                    objToUpdate.FechaFactura = o.FechaFactura;
                    objToUpdate.ARCHIVO_DE_ORIGEN_DE_TRANSACCI = o.ARCHIVO_DE_ORIGEN_DE_TRANSACCI;
                    objToUpdate.ESTADO_DE_COINCIDENCIA = o.ESTADO_DE_COINCIDENCIA;
                    objToUpdate.COINCIDENCIA_EN = o.COINCIDENCIA_EN;
                    objToUpdate.ING_FECHA = o.ING_FECHA;
                    objToUpdate.PRO_CODIGO = o.PRO_CODIGO;
                    objToUpdate.PRO_NOMBRE = o.PRO_NOMBRE;
                    objToUpdate.CI_CODIGO = o.CI_CODIGO;
                    objToUpdate.CRI_DESCRIPCION = o.CRI_DESCRIPCION;
                    objToUpdate.FOLTDA_CODIGO = o.FOLTDA_CODIGO;
                    objToUpdate.FOLIOCC = o.FOLIOCC;
                    objToUpdate.ING_SINCONTRIBUCION = o.ING_SINCONTRIBUCION;
                    objToUpdate.ING_IMPORTE = o.ING_IMPORTE;
                    objToUpdate.ING_CARGO = o.ING_CARGO;
                    objToUpdate.ING_CONTRIBUCION = o.ING_CONTRIBUCION;
                    objToUpdate.ING_REFERENCIA = o.ING_REFERENCIA;
                    objToUpdate.DIS_CONTABLE = o.DIS_CONTABLE;
                    objToUpdate.CENTROCOSTO = o.CENTROCOSTO;
                    objToUpdate.REGIONCONTABLE = o.REGIONCONTABLE;
                    objToUpdate.ZONACONTABLE = o.ZONACONTABLE;

                    await ctx.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                resp.Response = TypeResponse.ERROR;
                string message = ((ex.InnerException as UpdateException)?.InnerException as SqlException)?.Message;
                resp.Message = string.IsNullOrEmpty(message) ? ex.Message : message;
                resp.StackTrace = ex.StackTrace;
            }
            return resp;
        }

        public async Task<GenericResponse> sql(string query)
        {
            GenericResponse resp = new GenericResponse();
            try
            {
                using (TransactionMachineCtx ctx = new TransactionMachineCtx())
                {
                    int responseQuery = await ctx.Database.ExecuteNonQueryAsync(query);
                }
            }
            catch (Exception ex)
            {
                resp.Response = TypeResponse.ERROR;
                string message = ((ex.InnerException as UpdateException)?.InnerException as SqlException)?.Message;
                resp.Message = string.IsNullOrEmpty(message) ? ex.Message : message;
                resp.StackTrace = ex.StackTrace;
            }
            return resp;
        }
    }
}
