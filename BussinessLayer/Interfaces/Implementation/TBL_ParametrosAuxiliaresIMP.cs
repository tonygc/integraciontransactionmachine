﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BussinessLayer.Interfaces.Definition;
using BussinessLayer.OutputResponses;
using DAL.ViewModels;
using DAL.DataContext;

namespace BussinessLayer.Interfaces.Implementation
{
    public class TBL_ParametrosAuxiliaresIMP : ITBL_ParametrosAuxiliares
    {
        public async Task<TBL_ParametrosAuxiliaresResponse> getById(long id)
        {
            TBL_ParametrosAuxiliaresResponse resp = new TBL_ParametrosAuxiliaresResponse();
            try
            {
                using (TransactionMachineCtx ctx = new TransactionMachineCtx())
                {
                    resp.Data = ctx.TBL_ParametrosAuxiliares.Where(a => a.IdParametro == id).ToList();
                }
            }
            catch (Exception ex)
            {
                resp.Response = TypeResponse.ERROR;
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
            }
            return resp;
        }

        public async Task<TBL_ParametrosAuxiliaresResponse> getByGrupoAndValo2(string grupo, string valor2 = null)
        {
            TBL_ParametrosAuxiliaresResponse resp = new TBL_ParametrosAuxiliaresResponse();
            try
            {
                using (TransactionMachineCtx ctx = new TransactionMachineCtx())
                {
                    resp.Data = ctx.TBL_ParametrosAuxiliares.Where(a => a.Grupo.Trim() == grupo.Trim()
                    &&
                    a.Valor2 == valor2
                    ).ToList();
                }
            }
            catch (Exception ex)
            {
                resp.Response = TypeResponse.ERROR;
                resp.Message = ex.Message;
                resp.StackTrace = ex.StackTrace;
            }
            return resp;
        }
    }
}
