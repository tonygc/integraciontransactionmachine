﻿using BussinessLayer.OutputResponses;
using DAL.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.Interfaces.Definition
{
    public interface ITBL_Pagos
    {
        Task<BigintResponse> save(TBL_Pagos o);
        Task<GenericResponse> update(TBL_Pagos o);
        Task<TBL_PagosResponse> get(DateTime f1, DateTime f2);
        Task<TBL_PagosResponse> getById(long id);
        Task<GenericResponse> bulkInsert(IEnumerable<TBL_Pagos> o);
        Task<GenericResponse> bulkUpdate(IEnumerable<TBL_Pagos> o);
        Task<GenericResponse> sql(string query);
    }
}
