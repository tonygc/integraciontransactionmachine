﻿using BussinessLayer.OutputResponses;
using DAL.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.Interfaces.Definition
{
    public interface ITBL_ParametrosAuxiliares
    {
        Task<TBL_ParametrosAuxiliaresResponse> getById(long id);
        Task<TBL_ParametrosAuxiliaresResponse> getByGrupoAndValo2(string grupo, string valor2 = null);
    }
}
