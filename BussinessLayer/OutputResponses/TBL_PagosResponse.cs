﻿using DAL.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.OutputResponses
{
    public class TBL_PagosResponse : GenericResponse
    {
        private List<TBL_Pagos> _data { get; set; }
        public List<TBL_Pagos> Data { get { return _data; } set { _data = value ?? new List<TBL_Pagos>(); } }
    }
}
