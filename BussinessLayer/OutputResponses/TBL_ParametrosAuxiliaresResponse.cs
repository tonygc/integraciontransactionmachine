﻿using DAL.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.OutputResponses
{
    public class TBL_ParametrosAuxiliaresResponse : GenericResponse
    {
        public List<TBL_ParametrosAuxiliares> Data { get; set; }
        public TBL_ParametrosAuxiliaresResponse()
        {
            Data = new List<TBL_ParametrosAuxiliares>();
        }
    }
}
