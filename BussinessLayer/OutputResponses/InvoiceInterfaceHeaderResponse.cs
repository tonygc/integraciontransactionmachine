﻿using BussinessLayer.OutputResponses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.OutputResponses
{
    public class Message
    {
        public string code { get; set; }
        public string message { get; set; }
        public string severity { get; set; }
    }

    public class InvoiceInterfaceLine
    {
        public int LineNumber { get; set; }
    }

    public class Value
    {
        public long InvoiceId { get; set; }
        public string Status { get; set; }
        public List<InvoiceInterfaceLine> InvoiceInterfaceLine { get; set; }
    }

    public class Result
    {
        public Message Message { get; set; }
        public Value Value { get; set; }
    }

    public class InvoiceInterfaceResponse
    {
        public Result result { get; set; }
    }

    public class InvoiceInterfaceResponseINT : GenericResponse
    {
        public InvoiceInterfaceResponse InvoiceInterfaceResponse { get; set; }
    }
}
