﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.OutputResponses
{
    public class LoginResponse : GenericResponse
    {
        public DataLogin Data { get; set; }
    }
    public class DataLogin
    {
        public string Nombre { get; set; }
        public string Grupo { get; set; }
        public string Correo { get; set; }
        public string Error { get; set; }
        public string User { get; set; }
        public bool EsValido { get; set; }
        public ExtensionDataObject ExtensionData { get; set; }
    }

}
