﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.OutputResponses
{
    public enum TypeResponse
    {
        OK = 1,
        ERROR = 0
    }
    public class GenericResponse
    {
        public TypeResponse Response { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }

        public GenericResponse()
        {
            Response = TypeResponse.OK;
            Message = string.Empty;
            StackTrace = string.Empty;
        }
    }
}
