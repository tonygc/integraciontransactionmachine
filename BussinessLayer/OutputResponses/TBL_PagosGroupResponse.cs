﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.OutputResponses
{
    public class TBL_PagosGroup
    {
        public string id { get; set; }
        public string Estatus { get; set; }
        private bool? _facturar { get; set; }
        public bool Facturar { get { return _facturar ?? true; } set { _facturar = value; } }
        public DateTime COINCIDENCIA_EN { get; set; }
        public string PRO_CODIGO { get; set; }
        public string CI_CODIGO { get; set; }
        public string DIS_CONTABLE { get; set; }
        public string InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal IMPORTE { get; set; }
        public long[] IDS { get; set; }
    }
    public class TBL_PagosGroupResponse : GenericResponse
    {
        public List<TBL_PagosGroup> Data { get; set; }
    }
}
