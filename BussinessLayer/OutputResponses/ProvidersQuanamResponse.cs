﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BussinessLayer.OutputResponses
{
    public class ProvidersQuanamResponse : GenericResponse
    {
        public DATADS DATA_DS { get; set; }
    }

    public class SITIOS
    {
        public object ID_SITIO { get; set; }
        public string SITIO { get; set; }
        public string UNIDAD_COMPRA { get; set; }
        public string CORREO_COMPRA { get; set; }
        public string CORREO_PORTAL { get; set; }
        public string RETENER_PAGO { get; set; }
        public string BANCO { get; set; }
        public string SUCURSAL { get; set; }
        public string CUENTA { get; set; }
        public string CUENTA_ANTICIPO { get; set; }
        public string ID_CONTACTO { get; set; }
        public string MONEDAF { get; set; }
        public string CLAVE_CIE { get; set; }
        public string CONDICIONES_PAGO { get; set; }
        public string METODO_PAGO { get; set; }
    }

    public class PO11
    {
        public string NUMERO_PROVEEDOR { get; set; }
        public string NOMBRE_PROVEEDOR { get; set; }
        public string RFC_PROVEEDOR { get; set; }
        public string TIPO_PROVEEDOR { get; set; }
        public string TIPO_ORGFISCAL { get; set; }
        public string STATUS_PROVEEDOR { get; set; }
        public string TIPO_PUBLICACION { get; set; }
        public DateTime FECHACREACION { get; set; }
        public string USUARIOCREADOR { get; set; }
        public DateTime FECHAMODIFICACION { get; set; }
        public string USUARIOMOD { get; set; }
        public string PAIS { get; set; }
        public string DIRECCION { get; set; }
        public string ESTADO { get; set; }
        public string CP { get; set; }
        public string ZONA { get; set; }
        public string SUBZONA { get; set; }
        public string TELEFONO { get; set; }
        public string FAX { get; set; }
        public string Nombre_Alternativo { get; set; }
        public object VENDOR_ID { get; set; }
        public List<SITIOS> SITIOS { get; set; }
    }

    public class DATADS
    {
        public List<PO11> PO_11 { get; set; }
    }

    
}
