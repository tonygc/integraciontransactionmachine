﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BussinessLayer.InputRequests;
using DAL.ViewModels;
namespace webAPI
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize((config) =>
            {
                config.CreateMap<paymentDetail, TBL_Pagos>().ReverseMap();
                    //.ForMember(dest => dest.Email, opt => opt.Ignore())
                    //.ReverseMap();
                });
        }
    }
}