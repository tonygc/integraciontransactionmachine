﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using BussinessLayer.OutputResponses;
using BussinessLayer.InputRequests;
using System.Data.SqlClient;
using System.Data.Entity.Core;
using UtileriasIntegracionTM;
using RandomSolutions;
using System.IO;
using DAL.ViewModels;
using AutoMapper;
using BussinessLayer.Interfaces.Implementation;
using BussinessLayer.Interfaces.Definition;
using System.ComponentModel.DataAnnotations;
using ClosedXML.Excel;
using System.Net.Mime;
using System.Drawing;

namespace webAPI.Controllers
{
    [RoutePrefix("api/providers")]
    public class ProvidersController : ApiController
    {
        [Route("paymentEmail")]
        [HttpPost]
        public async Task<GenericResponse> save(paymentEmailRequest o)
        {
            GenericResponse respSaveLayout = new GenericResponse();
            ITBL_Pagos tbpagos = new TBL_PagosIMP();
            ITBL_ParametrosAuxiliares tbparams = new TBL_ParametrosAuxiliaresIMP();
            try
            {

                var collectionPayments = new List<TBL_Pagos>();
                foreach (var pago in o.Payments)
                {
                    TBL_Pagos obj = Mapper.Map<paymentDetail, TBL_Pagos>(pago);
                    ValidationContext ctxValidation = new ValidationContext(obj);
                    Validator.ValidateObject(obj, ctxValidation, true);
                    //var responseSave = await tbpagos.save(obj);
                    collectionPayments.Add(obj);
                }

                var respBulk = await tbpagos.bulkInsert(collectionPayments);
                if (respBulk.Response != TypeResponse.OK)
                    respSaveLayout = respBulk;

                if (!o.sendEmail)
                    goto finish;

                var smtpServer = (await tbparams.getById(11))?.Data.FirstOrDefault()?.Valor;
                var sender = (await tbparams.getById(12))?.Data.FirstOrDefault()?.Valor;
                var passsender = (await tbparams.getById(13))?.Data.FirstOrDefault()?.Valor;
                var subject = (await tbparams.getById(14))?.Data.FirstOrDefault()?.Valor;
                var body = (await tbparams.getById(16))?.Data.FirstOrDefault()?.Valor;
                var ssl = (await tbparams.getById(17))?.Data.FirstOrDefault()?.Valor == "1";
                var port = int.Parse((await tbparams.getById(18))?.Data.FirstOrDefault()?.Valor);

                var providers =
                    o.Payments.GroupBy(a => new { a.PRO_CODIGO })
                    .Select(b => new { Provider = b.Key.PRO_CODIGO });

                var listServicesExcel =
                new List<AttatchementEmail>();
                foreach (var prov in providers)
                {
                    var ProviderName = o.Payments.Where(a => a.PRO_CODIGO == prov.Provider).FirstOrDefault()?.PRO_NOMBRE;

                    using (MemoryStream excelFile = new MemoryStream())
                    {
                        var workbook = new XLWorkbook();     //creates the workbook

                        listServicesExcel = new List<AttatchementEmail>();

                        var provider_service =
                            o.Payments.Where(a => a.PRO_CODIGO == prov.Provider)
                            .GroupBy(a => new { a.CI_CODIGO })
                            .Select(b => new { Service = b.Key.CI_CODIGO });

                        foreach (var serv in provider_service)
                        {
                            var ServiceName = o.Payments.Where(a => a.CI_CODIGO == serv.Service
                              &&
                              a.PRO_CODIGO == prov.Provider).FirstOrDefault()?.CRI_DESCRIPCION;

                            var dataList = o.Payments.Where(a => a.CI_CODIGO == serv.Service
                            &&
                            a.PRO_CODIGO == prov.Provider).Select(x => new
                            {
                                FECHA = x.ING_FECHA
                              ,
                                NOMBRE = x.PRO_NOMBRE
                              ,
                                DESCRIPCION = x.CRI_DESCRIPCION
                              ,
                                FOLIOCC = x.FOLIOCC
                              ,
                                IMPORTE = x.ING_SINCONTRIBUCION
                              ,
                                REFERENCIA = x.ING_REFERENCIA
                            });
                            string sheetName = ServiceName;
                            if (sheetName.Length >= 30)
                                sheetName = sheetName.Substring(0, 30);

                            var wsDetailedData = workbook.AddWorksheet(sheetName); //creates the worksheet with sheetname 'data'
                            wsDetailedData.Cell(1, 1).InsertTable(dataList);

                            wsDetailedData.Range($"E{dataList.Count() + 2}").FormulaA1 = $"=SUM(E2:E{dataList.Count() + 1})";

                            wsDetailedData.Cell(dataList.Count() + 2,4).Value = "TOTAL";
                            wsDetailedData.Cell(dataList.Count() + 2, 4).Style.Fill.BackgroundColor = XLColor.FromColor(Color.Red);
                            wsDetailedData.Cell(dataList.Count() + 2, 4).Style.Font.FontColor = XLColor.FromColor(Color.White);

                            wsDetailedData.Cell(dataList.Count() + 2, 5).Style.Fill.BackgroundColor = XLColor.FromColor(Color.Red);
                            wsDetailedData.Cell(dataList.Count() + 2, 5).Style.Font.FontColor = XLColor.FromColor(Color.White);

                            wsDetailedData.Cells($"E2:E{dataList.Count() + 2}").Style.NumberFormat.Format = "$0.00";
                            wsDetailedData.Cells($"E2:E{dataList.Count() + 2}").DataType = XLDataType.Number;

                            wsDetailedData.Cells($"A2:A{dataList.Count() + 1}").Style.DateFormat.Format = "dd/MMM/yyyy";
                            wsDetailedData.Cells($"A2:A{dataList.Count() + 1}").DataType = XLDataType.DateTime;

                            wsDetailedData.Column(1).Width = 20;
                            wsDetailedData.Column(2).Width = 30;
                            wsDetailedData.Column(3).Width = 30;
                            wsDetailedData.Column(4).Width = 15;
                            wsDetailedData.Column(5).Width = 15;
                            wsDetailedData.Column(6).Width = 20;
                            //workbook.SaveAs(System.Web.HttpContext.Current.Server.MapPath(@"~\pruebaExcel.xlsx"));

                            //}
                        }

                        workbook.SaveAs(excelFile
                                ,
                                new SaveOptions
                                {
                                    EvaluateFormulasBeforeSaving = true,
                                    GenerateCalculationChain = true,
                                    ValidatePackage = false
                                }
                                );

                        excelFile.Seek(0, System.IO.SeekOrigin.Begin);



                        listServicesExcel.Add(
                        new AttatchementEmail
                        {
                            Binary = excelFile,
                            Name = $"PagoDeServicios{DateTime.Now.ToString("ddMMyyyy")}.xls",
                            Type = new ContentType("application/vnd.ms-excel")
                        });


                        var dataProvider = await UtileriasIntegracionTM.QuanamKiosko.Providers.GetByProId(o.Payments.First().PRO_CODIGO.Trim());

                        if (dataProvider.Response == TypeResponse.ERROR)
                            throw new Exception($"Error al obtener información del proveedor. {dataProvider.Message}");

                        var emailsRecipients = new List<string> { };

                        dataProvider.DATA_DS.PO_11.FirstOrDefault()?.SITIOS.Select(a => a.CORREO_PORTAL).ToList().ForEach(a =>
                            a.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(b =>
                                emailsRecipients.Add(b.Trim())
                            )
                        );

                        //emailsRecipients = new List<string> { "ing.joseantonio.desarrollo@gmail.com" };

                        var respEmail = Email.SendBySTMP(
                            smtpServer
                            , sender
                            , passsender
                            , emailsRecipients
                            , subject
                            , body
                            , listServicesExcel
                            , port
                            , ssl
                            , true);

                        //code for error test
                        //if (ProviderName.Contains("HERBAL") || ProviderName.Contains("MEGACABLE"))
                        //{
                        //    respEmail = new GenericResponse();
                        //    respEmail.Response = TypeResponse.ERROR;
                        //    respEmail.Message = $"ERROR EMAIL TEST EN PROVEEDOR {prov} - {ProviderName}{Environment.NewLine}" +
                        //        $"¿Desea hacer cambios? No hay problema. Todavía tiene tiempo para ajustar su pedido si así lo requiere. " +
                        //        $"Tiene hasta el viernes a medianoche (horario zona centro de Estados Unidos) para hacer cambios por medio del Hub o llámenos " +
                        //        $"al 1-800-08-87262 de lunes a viernes de 8:00am a 10:00pm.";
                        //}

                        if (respEmail.Response != TypeResponse.OK)
                        {
                            respSaveLayout.Response = respEmail.Response;
                            respSaveLayout.Message += " " + respEmail.Message;
                        }
                    }
                }
            finish:;
            }
            catch (Exception ex)
            {
                respSaveLayout.Response = TypeResponse.ERROR;
                string message = ((ex.InnerException as UpdateException)?.InnerException as SqlException)?.Message;
                respSaveLayout.Message = string.IsNullOrEmpty(message) ? ex.Message : message;
                respSaveLayout.StackTrace = ex?.StackTrace;
            }
            return respSaveLayout;
        }
    }
}
