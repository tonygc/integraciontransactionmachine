﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtileriasIntegracionTM;
using RandomSolutions;
using System.IO;
using DAL.ViewModels;
using AutoMapper;
using BussinessLayer.Interfaces.Implementation;
using BussinessLayer.Interfaces.Definition;
using System.ComponentModel.DataAnnotations;
using BussinessLayer.OutputResponses;
using System.Threading.Tasks;
using BussinessLayer.InputRequests;

namespace webAPI.Controllers
{
    [RoutePrefix("api/payments")]
    public class PaymentsController : ApiController
    {
        [Route("get")]
        [HttpGet]
        public async Task<TBL_PagosGroupResponse> get(DateTime f1, DateTime f2)
        {
            TBL_PagosGroupResponse respPayments = new TBL_PagosGroupResponse();
            ITBL_Pagos tbpagos = new TBL_PagosIMP();
            try
            {
                var payments = await tbpagos.get(f1, f2);
                respPayments.Data = payments.Data.GroupBy(a =>
                 new { COINCIDENCIA_EN = a.COINCIDENCIA_EN.Date, a.PRO_CODIGO, a.PRO_NOMBRE, a.CI_CODIGO, a.CRI_DESCRIPCION, a.DIS_CONTABLE, a.InvoiceId, a.InvoiceNumber })
                    .Select(a =>
                    new TBL_PagosGroup
                    {
                        id = $"{a.Key.COINCIDENCIA_EN.ToString("yyyyMMdd")}_{a.Key.PRO_CODIGO}_{a.Key.CI_CODIGO}_{a.Key.DIS_CONTABLE}"
                    ,
                        COINCIDENCIA_EN = a.Key.COINCIDENCIA_EN
                    ,
                        PRO_CODIGO = $"{a.Key.PRO_CODIGO} - {a.Key.PRO_NOMBRE}"
                    ,
                        CI_CODIGO = $"{a.Key.CI_CODIGO} - {a.Key.CRI_DESCRIPCION}"
                    ,
                        DIS_CONTABLE = a.Key.DIS_CONTABLE
                    ,
                        IMPORTE = a.Sum(b => b.ING_SINCONTRIBUCION)
                    ,
                        IDS = a.Select(b => b.IdPago).Distinct().ToArray()
                    ,
                        InvoiceId = a.Key.InvoiceId ?? ""
                    ,
                        InvoiceNumber = a.Key.InvoiceNumber ?? null
                    }).ToList();
            }
            catch (Exception ex)
            {
                respPayments.Response = TypeResponse.ERROR;
                respPayments.Message = $"{ex.Message} {ex.InnerException?.Message}";
                respPayments.StackTrace = ex?.StackTrace;
            }
            return respPayments;
        }

        [Route("standarPassive")]
        [HttpPost]
        public async Task<InvoiceInterfaceResponseINT> standarPassive(StandarPassiveRequest o)
        {
            InvoiceInterfaceResponseINT respPayments = new InvoiceInterfaceResponseINT();
            ITBL_Pagos tbpagos = new TBL_PagosIMP();
            ITBL_ParametrosAuxiliares tbparametros = new TBL_ParametrosAuxiliaresIMP();
            int debug = 0;
            try
            {
                var payments = await tbpagos.getById(o.IDS.First());
                var payment = payments.Data.FirstOrDefault();
                InvoiceInterfaceHeaderRequest data = new InvoiceInterfaceHeaderRequest();

                InvoiceInterfaceHeader Interfaceheader = new InvoiceInterfaceHeader();

                Interfaceheader.OperatingUnit = (await tbparametros.getById(6))?.Data.FirstOrDefault().Valor;

                Interfaceheader.VendorNumber = int.Parse(payment?.PRO_CODIGO);
                Interfaceheader.VendorName = string.Empty;//payment?.PRO_NOMBRE;

                var sitioProv = (await tbparametros.getByGrupoAndValo2("SITIO", payment?.PRO_CODIGO)).Data;
                if (sitioProv.Count == 0)
                    sitioProv = (await tbparametros.getByGrupoAndValo2("SITIO")).Data;

                Interfaceheader.VendorSiteCode = sitioProv.Count == 0 ? "PRINCIPAL" : sitioProv.FirstOrDefault()?.Valor;

                Interfaceheader.Description = $"{payment?.CI_CODIGO} {payment?.CRI_DESCRIPCION}";
                Interfaceheader.GlDate = payment?.COINCIDENCIA_EN.ToString("yyyy-MM-dd");
                Interfaceheader.GlobalAttribute1 = string.Empty;//"UUID112";
                Interfaceheader.GroupId = string.Empty;//"GC_02";
                Interfaceheader.AttributeNumber1 = string.Empty;//"12233";

                var monedaProv = (await tbparametros.getByGrupoAndValo2("MONEDA", payment?.PRO_CODIGO)).Data;
                if (monedaProv.Count == 0)
                    monedaProv = (await tbparametros.getByGrupoAndValo2("MONEDA")).Data;
                Interfaceheader.InvoiceAmount = new InvoiceAmount
                {
                    Amount = Convert.ToDouble(o.AMOUNT),
                    currencyCode = monedaProv.Count == 0 ? "MXN" : monedaProv.FirstOrDefault()?.Valor
                };


                Interfaceheader.InvoiceCurrencyCode = monedaProv.Count == 0 ? "MXN" : monedaProv.FirstOrDefault()?.Valor;

                Interfaceheader.InvoiceDate = payment.COINCIDENCIA_EN.ToString("yyyy-MM-dd");

                var prefijoFactura = (await tbparametros.getById(19)).Data;

                Interfaceheader.InvoiceNumber = $"{prefijoFactura.FirstOrDefault()?.Valor}-{payment.COINCIDENCIA_EN.ToString("yyyyMMdd")}_{payment.PRO_CODIGO}_{payment.CI_CODIGO}";
                Interfaceheader.PrepayNumber = "";
                Interfaceheader.InvoiceTypeLookupCode = "STANDARD";

                var impuestoProv = (await tbparametros.getByGrupoAndValo2("IMPUESTO", payment?.PRO_CODIGO)).Data;
                if (impuestoProv.Count == 0)
                    impuestoProv = (await tbparametros.getByGrupoAndValo2("IMPUESTO")).Data;

                Interfaceheader.InvoiceInterfaceLine?.Add(
                    new
                    BussinessLayer.InputRequests.InvoiceInterfaceLine
                    {
                        LineNumber = 1,
                        Description = $"{payment?.CI_CODIGO} {payment?.CRI_DESCRIPCION}",
                        AccountingDate = payment.COINCIDENCIA_EN.ToString("yyyy-MM-dd"),
                        Amount = new _Amount
                        {
                            Amount = Convert.ToDouble(o.AMOUNT),
                            currencyCode = monedaProv.Count == 0 ? "MXN" : monedaProv.FirstOrDefault()?.Valor
                        },
                        AwtGroupName = string.Empty,//"COMISI_CORP",
                        DistCodeConcatenated = payment.DIS_CONTABLE,//"01-517041-19001-10-19-00-00000",
                        TaxClassificationCode = impuestoProv.Count == 0 ? "NOAFECTO_AP" : impuestoProv.FirstOrDefault()?.Valor//"IVA_16_AP",
                        //Description = $"{payment?.CI_CODIGO} {payment?.CRI_DESCRIPCION}"
                        //BudgetDate = o.DATE.ToString("yyyy-MM-dd")
                    }
                    );
                data.invoiceInterfaceHeader = Interfaceheader;

                respPayments = await UtileriasIntegracionTM.QuanamKiosko.Payments.GeneratePassive(data);
                debug = 1;
                //FACTURACION EXITOSA
                if (respPayments.InvoiceInterfaceResponse?.result?.Value?.Status == "PROCESSED" ||
                    respPayments.Message.ToUpper().Contains("DUPLICATE INVOICE NUMBER")
                    )
                {
                    var updPayments = new List<TBL_Pagos>();

                    debug = 2;
                    string query = $"UPDATE TBL_Pagos set " +
                        $" InvoiceId='{(respPayments.Message.ToUpper().Contains("DUPLICATE INVOICE NUMBER") ? 0 : respPayments.InvoiceInterfaceResponse?.result?.Value?.InvoiceId)}'" +
                        $",FechaFactura=GETDATE()" +
                        $",InvoiceNumber='{data.invoiceInterfaceHeader.InvoiceNumber}' " +
                        $"WHERE IdPago in ({string.Join(",", o.IDS)})";
                    var responseUpd = await tbpagos.sql(query);

                    debug = 3;
                    if (responseUpd.Response != TypeResponse.OK)
                        throw new Exception(responseUpd.Message);

                    respPayments.Response = TypeResponse.OK;
                }
                else
                {
                    debug = 4;
                    respPayments.Message = $"Error en Proveedor {payment.PRO_CODIGO} - {payment.PRO_NOMBRE}, " +
                        $"Servicio {payment.CI_CODIGO} - {payment.CRI_DESCRIPCION}, InvoiceNumber {data.invoiceInterfaceHeader.InvoiceNumber}, " +
                        $"Mensaje {respPayments.Message}";
                }

            }
            catch (Exception ex)
            {
                respPayments.Response = TypeResponse.ERROR;
                respPayments.Message = $"DEBUG:{debug} {ex.Message} {ex.InnerException?.Message}";
                respPayments.StackTrace = ex?.StackTrace;
            }
            return respPayments;
        }
    }
}
