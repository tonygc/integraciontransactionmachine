﻿using BussinessLayer.InputRequests;
using BussinessLayer.OutputResponses;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace webAPI.Controllers
{
    [RoutePrefix("api/users")]
    public class LoginController : ApiController
    {
        [Route("login")]
        [HttpPost]
        public async Task<LoginResponse> validateLogin(LoginRequest o)
        {
            LoginResponse respLogin = new LoginResponse();
            try
            {
                using (wcfLoginDominio.LoginClient wcfLogin = new wcfLoginDominio.LoginClient())
                {
                    wcfLoginDominio.Usuario usrDominio = wcfLogin.Autenticar(o.usr, o.pwd);

                    if (!usrDominio.EsValido) throw new Exception($"{usrDominio.Error}");

                    respLogin.Data = new DataLogin
                    {
                        Correo = usrDominio.Correo,
                        Error = usrDominio.Error,
                        EsValido = usrDominio.EsValido,
                        ExtensionData = usrDominio.ExtensionData,
                        Grupo = usrDominio.Grupo,
                        Nombre = usrDominio.Nombre,
                        User = usrDominio.User
                    };
                }
            }
            catch (Exception ex)
            {
                respLogin.Response = TypeResponse.ERROR;
                string message = ((ex.InnerException as UpdateException)?.InnerException as SqlException)?.Message;
                respLogin.Message = string.IsNullOrEmpty(message) ? ex.Message : message;
                respLogin.StackTrace = ex?.StackTrace;
            }
            return respLogin;
        }
    }
}
