 /**
 * LG
 *
 * Main controller.js file
 * Define controllers with data used in Inspinia theme
 *
 */

/**
 * MainCtrl - controller
 * Contains several global data used in different view
 *
 */
function MainCtrl($http) {

    /**
     * slideInterval - Interval for bootstrap Carousel, in milliseconds:
     */
    this.slideInterval = 5000;

};

/**
 * translateCtrl - Controller for translate
 */
function translateCtrl($translate, $scope) {
    $scope.changeLanguage = function (langKey) {
        $translate.use(langKey);
        $scope.language = langKey;
    };
    $scope.language = 'es';
}

/**
    * Controller de la App
    */
var usuarioTL = "null";

function accesoCtrl($scope, $http, $location, $rootScope, $timeout, $stateParams, accesoService) {
    accesoService.clearCredentials();

    $scope.usr = $scope.pwd = null;

    $scope.msg = false;
    $scope.loading = false;
    $scope.login = true;

    $scope.usr = "";
    $scope.pwd = "";

    $scope.acceso = function () {
        $scope.msg = false;
        $scope.loading = true;

        accesoService.accesoUser($scope.usr, $scope.pwd,
            function (response) {
                $timeout(function () {
                    var user = response;

                    if (user.Data != null) {
                        $scope.msg = { mensaje: 'Acceso autorizado.', tipo: 'alert-success', icon: 'fa fa-check-circle' };
                        //$scope.loading = false;
                        $timeout(function () {
                            accesoService.setCredentials(user.Data);
                            $location.path('/home/carga');
                        }, 1000);
                    }
                    else {
                        $scope.msg = { mensaje: user.Message, tipo: 'alert-warning', icon: 'fa fa-exclamation-triangle' };
                        $scope.loading = false;
                        //$timeout(function () {
                        //    $location.path('/home/carga');
                        //}, 1000);
                    }
                }, 1000);
            },
            function (response) {
                $timeout(function () {

                    if (response == null) {
                        $scope.msg = { mensaje: 'Error en el servicio. Contacte al área de sistemas.', tipo: 'alert-warning', icon: 'fa fa-exclamation-circle' };
                        $scope.loading = false;
                        return;
                    }

                    if (response.status == 200) {
                        var user = response.data;

                        if (user.Status == true) {
                            $scope.msg = { mensaje: 'Acceso autorizado.', tipo: 'alert-success', icon: 'fa fa-check-circle' };
                            $scope.loading = false;
                            $timeout(function () {
                                accesoService.setCredentials(user);
                                $location.path('/home/inicio');
                            }, 1000);
                        }
                        else {
                            $scope.msg = { mensaje: 'Acceso denegado, Usuario desactivado.', tipo: 'alert-warning', icon: 'fa fa-exclamation-triangle' };
                            $scope.loading = false;
                        }
                    }
                    else {
                        $scope.msg = { mensaje: 'Usuario / contraseña incorrecta.', tipo: 'alert-warning', icon: 'fa fa-exclamation-circle' };
                        $scope.loading = false;
                    }
                }, 1000);
            });
    };
}

function homeCtrl($scope, $http, $location, $rootScope, $cookieStore, $timeout, CONSTANTES) {

    $scope.permiso = function (perfil, opcion) {
        var p = false;
        if (CONSTANTES.PERMISOS[perfil].indexOf(opcion) != -1) {
            p = true;
        }
        return p;
    };

    $scope.cerrarSesion = function () {
        $rootScope.user = false;
        $cookieStore.remove('user');
        $location.path('/acceso');
    };
}

function inicioCtrl($scope, $http, $location, $rootScope, $timeout, CONSTANTES) {

}

function cargaCtrl($scope, $http, $location, $rootScope, $timeout, $translate, $filter, SweetAlert, CONSTANTES) {

    $scope.sendEmail = true;
    $scope.chk_todos = true;
    $scope.clickCarga = function () {
        setTimeout(function () {
            angular.element('#xlf').trigger('click');
            $scope.$apply();
        }, 0);
    };
    $scope.checkProveedor = [];
    $scope.isAllSelected = true;
    $scope.totalRegistros = 0;
    $scope.resultsLayout = { status: true, Mensajes: [] };

    function LimpiarContenedores() {
        $scope.keys = [];
        $scope.proveedores_enviar = [];
        $scope.proveedores = [];
        $scope.servicios = [];
        $scope.servicios_todos = [];
        $scope.layout = false;
        $scope.result = false;
        $scope.loading = false;

        $scope.contExiste = [];
        $scope.contError = [];
        $scope.contGuardados = [];

        $scope.conerror = false;
        $scope.archivoNoPermitido = false;
        $scope.archivoVacio = false;
        $scope.errorEncabezados = false;
        $scope.contenidoNoValido = false;

        $scope.revisados = false;

        $scope.resultsLayout = { status: true, Mensajes: [] };
    }

    LimpiarContenedores();

    $scope.cancelarCarga = function () {
        $scope.layout = false;
        $scope.conerror = false;
        $scope.result = false;
        $scope.sendEmail = true;
        $scope.archivoNoPermitido = false;
        $scope.contenidoNoValido = false;
        $scope.archivoVacio = false;
        $scope.errorEncabezados = false;
    };

    $scope.cancelarResultados = function () {
        LimpiarContenedores();
    };

    var numero = 0;
    $scope.setKeys = function (keys) {
        numero = 0;
        keys.forEach(function (item) {
            numero = numero + 1;

            if (numero == 1) {
                if (item == "ARCHIVO_DE_ORIGEN_DE_TRANSACCI") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE
                        NombreOK: "ARCHIVO_DE_ORIGEN_DE_TRANSACCI"
                    });
                }
            }
            if (numero == 2) {
                if (item == "ESTADO_DE_COINCIDENCIA") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE
                        NombreOK: "ESTADO_DE_COINCIDENCIA"
                    });
                }
            }
            if (numero == 3) {
                if (item == "COINCIDENCIA_EN") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE
                        NombreOK: "COINCIDENCIA_EN"
                    });
                }
            }
            if (numero == 4) {
                if (item == "ING_FECHA") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE
                        NombreOK: "ING_FECHA"
                    });
                }
            }
            if (numero == 5) {
                if (item == "PRO_CODIGO") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE,
                        NombreOK: "PRO_CODIGO"
                    });
                }
            }
            if (numero == 6) {
                if (item == "PRO_NOMBRE") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE,
                        NombreOK: "PRO_NOMBRE"
                    });
                }
            }
            if (numero == 7) {
                if (item == "CI_CODIGO") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE
                        NombreOK: "CI_CODIGO"
                    });
                }
            }
            if (numero == 8) {
                if (item == "CRI_DESCRIPCION") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE,
                        NombreOK: "CRI_DESCRIPCION"
                    });
                }
            }
            if (numero == 9) {
                if (item == "FOLTDA_CODIGO") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE,
                        NombreOK: "FOLTDA_CODIGO"
                    });
                }
            }
            if (numero == 10) {
                if (item == "FOLIOCC") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE,
                        NombreOK: "FOLIOCC"
                    });
                }
            }
            if (numero == 11) {
                if (item == "ING_IMPORTE") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE,
                        NombreOK: "ING_IMPORTE"
                    });
                }
            }
            if (numero == 12) {
                if (item == "ING_CARGO") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE,
                        NombreOK: "ING_CARGO"
                    });
                }
            }
            if (numero == 13) {
                if (item == "ING_SINCONTRIBUCION") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE,
                        NombreOK: "ING_SINCONTRIBUCION"
                    });
                }
            }
            if (numero == 14) {
                if (item == "ING_REFERENCIA") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE,
                        NombreOK: "ING_REFERENCIA"
                    });
                }
            }
            if (numero == 15) {
                if (item == "DIS_CONTABLE") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE,
                        NombreOK: "DIS_CONTABLE"
                    });
                }
            }
            if (numero == 16) {
                if (item == "CENTROCOSTO") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE,
                        NombreOK: "CENTROCOSTO"
                    });
                }
            }
            if (numero == 17) {
                if (item == "REGIONCONTABLE") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE,
                        NombreOK: "REGIONCONTABLE"
                    });
                }
            }
            if (numero == 18) {
                if (item == "ZONACONTABLE") {
                    $scope.keys.push({
                        key: item,
                        valid: true,
                        NombreOK: ""
                    });
                }
                else {
                    $scope.keys.push({
                        key: item,
                        valid: true, //SHOULD BE FALSE,
                        NombreOK: "ZONACONTABLE"
                    });
                }
            }
        });

    };

    $scope.setFile = function (file) {
        $scope.file = file;
    };

    $scope.noValidos = function (v1, v2, v3, v4, v5) {
        if (v1 == true) $("#id_conerror").removeClass("ng-hide");
        if (v2 == true) $("#id_archivoNoPermitido").removeClass("ng-hide");
        if (v3 == true) $("#id_contenidoNoValido").removeClass("ng-hide");
        if (v4 == true) $("#id_archivoVacio").removeClass("ng-hide");
        if (v5 == true) $("#id_errorEncabezados").removeClass("ng-hide");

        $scope.conerror = v1;
        $scope.archivoNoPermitido = v2;
        $scope.contenidoNoValido = v3;
        $scope.archivoVacio = v4;
        $scope.errorEncabezados = v5;
    };

    $scope.cargarXLS = function (elementos) {

        $scope.cancelarCarga();
        LimpiarContenedores();
        $scope.layout = elementos;
        $scope.invalidos = 0;
        $scope.checkProveedor = [];
        try {

            elementos.forEach(function (item) {
                //validar si ya existe ese proveedor
                existsProve = false;
                valido = true;
                totalImporte = 0;

                if (item["PRO_CODIGO"] == undefined) {
                    valido = false; //no mostrar
                    $scope.invalidos += 1;
                } else {
                    //buscar
                    var index = 0;
                    var indexBorrar = -1;
                    $scope.proveedores.forEach(function (elem) {
                        if (elem["PRO_CODIGO"] == item["PRO_CODIGO"]) {
                            existsProve = true;
                            totalImporte = parseFloat(resetItem(elem["TOTAL_IMPORTE"])) + parseFloat(resetItem(item["ING_SINCONTRIBUCION"]));
                            indexBorrar = index;
                        }
                        index += 1;
                    });
                    if (indexBorrar != -1) {
                        $scope.proveedores.splice(indexBorrar, 1);
                    }
                }

                if (valido) {
                    if (!existsProve) {
                        $scope.checkProveedor.push(item["PRO_CODIGO"]);
                        totalImporte = parseFloat(resetItem(item["ING_SINCONTRIBUCION"]));
                    }
                    $scope.proveedores.push({
                        PRO_CODIGO: item["PRO_CODIGO"],
                        PRO_NOMBRE: item["PRO_NOMBRE"],
                        TOTAL_IMPORTE: parseFloat(totalImporte).toFixed(2)
                    });
                }

                $scope.servicios_todos.push({
                    ARCHIVO_DE_ORIGEN_DE_TRANSACCI: item["ARCHIVO_DE_ORIGEN_DE_TRANSACCI"],
                    ESTADO_DE_COINCIDENCIA: item["ESTADO_DE_COINCIDENCIA"],
                    COINCIDENCIA_EN: item["COINCIDENCIA_EN"],
                    ING_FECHA: resetItem(item["ING_FECHA"]),
                    PRO_CODIGO: resetItem(item["PRO_CODIGO"]),
                    PRO_NOMBRE: resetItem(item["PRO_NOMBRE"]),
                    CI_CODIGO: item["CI_CODIGO"],
                    CRI_DESCRIPCION: resetItem(item["CRI_DESCRIPCION"]),
                    FOLTDA_CODIGO: item["FOLTDA_CODIGO"],
                    FOLIOCC: resetItem(item["FOLIOCC"]),
                    ING_SINCONTRIBUCION: resetItem(item["ING_SINCONTRIBUCION"]),
                    ING_IMPORTE: item["ING_IMPORTE"],
                    ING_CARGO: item["ING_CARGO"],
                    ING_CONTRIBUCION: item["ING_CONTRIBUCION"],
                    ING_REFERENCIA: resetItem(item["ING_REFERENCIA"]),
                    DIS_CONTABLE: item["DIS_CONTABLE"],
                    CENTROCOSTO: item["CENTROCOSTO"],
                    REGIONCONTABLE: item["REGIONCONTABLE"],
                    ZONACONTABLE: item["ZONACONTABLE"]
                });
            });
        }
        catch (e) {
            console.log(e);
            $scope.contenidoNoValido = true;
            $scope.conerror = true;
        }

        $scope.layout = elementos;
        $scope.totalRegistros = $scope.layout.length - $scope.invalidos;
    };

    $scope.getCountServicios = function (strCod) {
        return filterFilter($scope.servicios, { PRO_CODIGO: strCod }).length;
    };

    $scope.getCountTotalServicios = function (strCod) {
        return filterFilter($scope.servicios_todos, { PRO_CODIGO: strCod }).length;
    };

    $scope.cargandoServicios = function (strCod) {
        return getCountServicios(strCod) < getCountTotalServicios(strCod);
    };

    $scope.toggleServices = function (proveedor) {
        var counter = 0;
        $scope.servicios_todos.forEach(function (servicio) {
            //buscar si existe
            var index = $scope.servicios.indexOf(servicio);
            if (index == -1) {
                if (servicio["PRO_CODIGO"] == proveedor) {
                    if (counter < 50) {
                        $scope.servicios.push(servicio);
                        counter += 1;
                    }
                }
            } else {
                if ($scope.servicios[index]["PRO_CODIGO"] == proveedor) {
                    $scope.servicios.splice(index, 1);
                }
            }
        });
    };

    $scope.guardarLayout = function () {
        $scope.loading = true;
        $scope.progress_bar_advance = 0;
        SweetAlert.swal({
            title: $filter('translate')('TITLE_ENVIAR_FACTURAS'),
            text: $scope.sendEmail ? $filter('translate')('TEXT_GUARDAR_LAYOUT') : $filter('translate')('TEXT_GUARDAR_LAYOUT2'),
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#1ab394",
            confirmButtonText: $filter('translate')('CONFIRM_BUTTON_TEXT'),
            cancelButtonText: $filter('translate')('CANCEL_BUTTON_TEXT'),
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    $scope.guardarPayments();
                } else {
                    swal.close();
                    $scope.loading = false;
                }
            });
    };

    $scope.guardarPayments = function () {

        $scope.progress_bar_advance = 0;
        $scope.total_revisados = 0;
        $scope.result = false;
        swal.close();

        $scope.checkProveedor.forEach(function (item) {

            services_to_send = [];
            $scope.resultsLayout = { status: true, Mensajes: [] };
            $scope.servicios_todos.forEach(function (servicio) {
                if (servicio["PRO_CODIGO"] == item) {
                    services_to_send.push(servicio);
                }
            });

            var req = {
                method: 'POST',
                url: CONSTANTES.RUTA_API + CONSTANTES.API_PAYMENTS_EMAIL,
                headers: {
                    'Accept': "application/json, charset=utf-8"
                },
                data: { sendEmail: $scope.sendEmail, Payments: services_to_send }
            };

            $http(req).then(function (data) {
                $timeout(function () {
                    if (data.data.Response == 0) {
                        $scope.resultsLayout.Mensajes.push(data.data.Message);
                    }

                    $scope.progress_bar_advance = $scope.progress_bar_advance + 1;
                    $scope.total_revisados = $scope.total_revisados + services_to_send.length;

                    if ($scope.progress_bar_advance == $scope.checkProveedor.length) {
                        $scope.finishEnviados($scope.total_revisados);
                    }

                }, 1000);
            }, function () {
                $scope.resultsLayout = { status: false, Mensajes: ["ERROR AL REALIZAR LA PETICION"] };
                $scope.loading = false;
            });

        });

    };

    $scope.finishEnviados = function (revisados) {
        $scope.loading = false;
        //$scope.layout = false;
        $scope.result = true;

        if ($scope.resultsLayout.Mensajes.length > 0) {
            $scope.resultsLayout.status = false;
        }

        $scope.revisados = revisados;

        if ($scope.resultsLayout.status) {
            SweetAlert.swal({
                title: $filter('translate')('INFORMATION'),
                text: $scope.progress_bar_advance + $filter('translate')('PROCESSED_LAYOUT'),
                type: "success"
            });
        } else {
            SweetAlert.swal({
                title: $filter('translate')('INFORMATION'),
                text: $scope.resultsLayout.Mensajes.length+" de "+ $scope.progress_bar_advance + " layouts procesado con errores.",
                type: "warning"
            });
        }
    };

    $scope.toggleAll = function (isAllSelected) {
        angular.forEach($scope.proveedores, function (itm) {
            itm.selected = isAllSelected;
            $scope.checkedProveedores(itm);
        });
    };

    $scope.optionToggled = function () {
        $scope.isAllSelected = $scope.proveedores.every(function (itm) {
            return itm.selected;
        });
    };

    $scope.checkedProveedores = function (item) {
        var index = $scope.checkProveedor.indexOf(item.PRO_CODIGO);
        if (item.selected) {
            if (index == -1) {
                $scope.checkProveedor.push(item.PRO_CODIGO);
            }
        } else {
            if (index != -1) {
                $scope.checkProveedor.splice(index, 1);
            }
        }
    };

    $scope.isCheckedProveedores = function (item) {
        return $scope.checkProveedor.indexOf(item.PRO_CODIGO) != -1;
    };
}

function facturasCtrl($scope, $http, $location, $rootScope, $timeout, $q, $filter, DTOptionsBuilder, SweetAlert, CONSTANTES) {
    $scope.loading = false;
    $scope.hay_facturas = false;
    $scope.facturas = [];
    $scope.errores = [];
    $scope.correctos = [];
    $scope.daterange =
        moment().format('DD/MM/YYYY') + ' - ' + moment().add(10, 'days').format('DD/MM/YYYY');

    $scope.chk_todos = true;
    $scope.checkInvoice = [];

    $scope.isAllSelected = false;
    $scope.result = false;
    $scope.totalProccesed = 0;
    $("#daterange").val(moment().add(-1, 'days').format('DD/MM/YYYY') + " - " + moment().format('DD/MM/YYYY'));

    $scope.buscarFacturas = function () {

        $scope.hay_facturas = false;
        $scope.loadingFacturar = true;
        $scope.loading = true;
        var fechas = $("#daterange").val().split("-");
        $scope.fecha1 = moment(fechas[0].trim(), 'DD/MM/YYYY').format('YYYY-MM-DD');
        $scope.fecha2 = moment(fechas[1].trim(), 'DD/MM/YYYY').format('YYYY-MM-DD');

        $http.get(CONSTANTES.RUTA_API + CONSTANTES.API_FACTURAS + "f1=" + $scope.fecha1 + "&f2=" + $scope.fecha2)
            .then(function (response) {
                $scope.hay_facturas = response.data.Data.length > 0;
                $scope.facturas = response.data.Data;
                //id = 1;
                //$scope.facturas.forEach(function (item) {
                //    item.id = id;
                //    id += 1;
                //});
                var auxChecks = $scope.checkInvoice.slice();;
                for (i = 0; i < auxChecks.length; i++) {
                    var existsId = false;
                    $scope.facturas.forEach(function (factura) {
                        if (factura.id == auxChecks[i]) {
                            existsId = true;
                        }
                    });
                    if (!existsId)
                    {
                        $scope.checkInvoice.splice($scope.checkInvoice.indexOf(auxChecks[i]), 1);
                    }
                }

                //$scope.result = false;
                $scope.loading = false;
                $scope.loadingFacturar = false;
            }, function (response) {
                //$scope.result = false;
                $scope.loading = false;
            });

    };

    $scope.buscarFacturas();

    $scope.enviarFacturas = function () {
        $scope.loadingFacturar = true;
        $scope.progress_bar_advance = 0;
        $scope.result = false;
        if ($scope.checkInvoice.length > 0) {
            SweetAlert.swal({
                title: $filter('translate')('TITLE_ENVIAR_FACTURAS'),
                text: $filter('translate')('TEXT_ENVIAR_FACTURAS'),
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#1ab394",
                confirmButtonText: $filter('translate')('CONFIRM_BUTTON_TEXT'),
                cancelButtonText: $filter('translate')('CANCEL_BUTTON_TEXT'),
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    if (isConfirm) {
                        $scope.enviarFacturasConfirmado();
                    } else {
                        swal.close();
                        $scope.loadingFacturar = false;
                    }
                });
        } else {
            SweetAlert.swal({
                title: $filter('translate')('ATENTION'),
                text: $filter('translate')('SELECT_ONE_INVOICE'),
                type: "warning",
                confirmButtonColor: "#1ab394"
            });
            $scope.loadingFacturar = false;
        }
    };

    $scope.enviarFacturasConfirmado = function () {

        $scope.progress_bar_advance = 0;
        $scope.total_revisados = 0;
        $scope.errores = [];
        $scope.correctos = [];
        invoices_to_send = [];
        $scope.result = false;
        swal.close();

        $scope.facturas.forEach(function (item) {

            if ($scope.checkInvoice.indexOf(item.id) > -1) {
                invoices_to_send.push(item);

                if (invoices_to_send.length == $scope.checkInvoice.length) {
                    $scope.callback(invoices_to_send);
                }
            }

        });

    };

    $scope.callback = function (invoices_to_send) {
        // Creating an empty initial promise that always resolves itself.
        var promise = $q.all([]);

        // Iterating list of items.
        angular.forEach(invoices_to_send, function (item) {
            promise = promise.then(function () {
                return $timeout(function () {
                    $scope.progress_bar_advance += 1;
                    $scope.envioFactura(item);
                }, 2000);
            });
        });

        promise.finally(function () {
            console.log('Chain finished!');
            //$scope.finishEnviados(invoices_to_send.length);
        });
    };

    $scope.envioFactura = function (item) {
        data_enviar = {
            AMOUNT: item["IMPORTE"],
            DATE: moment().format("YYYY-MM-DD"),
            IDS: item["IDS"]
        };
        var req = {
            method: 'POST',
            url: CONSTANTES.RUTA_API + CONSTANTES.API_FACTURAR,
            headers: {
                'Accept': "application/json, charset=utf-8"
            },
            data: data_enviar,
            async: true
        };

        $http(req).then(function (response) {
            if (response.data.Response != 1) {
                $scope.errores.push(
                    {
                        id: data_enviar.id,
                        mensaje: response.data.Message
                    }
                );
            } else {
                $scope.correctos.push(item);
            }

            if (invoices_to_send.length == ($scope.correctos.length + $scope.errores.length)) {
                $scope.finishEnviados(invoices_to_send.length);
            }

        }, function () {
            $scope.errores.push(
                {
                    id: data_enviar.IDS,
                    mensaje: "Error desconocido, consulte a su encargado de sistemas."
                }
            );
        });

    };

    $scope.finishEnviados = function (revisados) {
        //implementar que espere respuesta
        //if (revisados == $scope.errores.length + $scope.correctos.length) {
        $scope.totalProccesed = $scope.checkInvoice.length;
        $scope.checkInvoice = [];
        $scope.buscarFacturas();
        $timeout(function () {
            $scope.loadingFacturar = false;
            //$scope.hay_facturas = false;
            $scope.result = true;
            $scope.revisados = revisados;
            SweetAlert.swal({
                title: $filter('translate')('INFORMATION'),
                text: $scope.revisados + $filter('translate')('PROCESSED_INVOICES'),
                type: ($scope.errores.length==0?"success":"warning")
            });
            esperar = false;
        }, 2000);
        //}
    };

    $scope.toggleAll = function (isAllSelected) {
        angular.forEach($scope.facturas, function (itm) {
            if (itm.InvoiceId != "") {
                itm.selected = false;
            } else {

                itm.selected = isAllSelected;
                $scope.checkedInvoices(itm);
            }
        });
    };

    $scope.optionToggled = function () {
        $scope.isAllSelected = $scope.facturas.every(function (itm) {
            return itm.selected;
        });
    };

    $scope.checkedInvoices = function (item) {
        var index = $scope.checkInvoice.indexOf(item.id);
        
        if (item.selected) {
            if (index == -1) {
                $scope.checkInvoice.push(item.id);
            }
        } else {
            if (index != -1) {
                $scope.checkInvoice.splice(index, 1);
            }
        }
    };

    $scope.isCheckedInvoices = function (item) {
        return $scope.checkInvoice.indexOf(item.id) != -1;
    };

    $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('bFilter', false)
        .withOption('bPaginate', false)
        .withLanguage(CONSTANTES._languageESdataTable);
}

function modalCtrl($scope, $uibModal) {

    $scope.openModal = function (view, ctrl, size, clss, parentScope) {
        $scope.parentCtrl = parentScope;
        var modalInstance = $uibModal.open({
            templateUrl: 'views/' + view + '.html',
            size: size,
            controller: ctrl,
            windowClass: clss,
            scope: $scope,
            bindToController: true,
            controllerAs: 'modalCtrl',
            backdrop: 'static',
            keyboard: false
        });
    };
}

//funcion para formatear dato de columna excel que viene con signo de pesos
function resetItem(item) {
    var value = item != undefined ? item.toString().replace('$', '').trim() : "0";
    return (value.length > 0) ? value : "0";
}

function utf8(item) {
    var value = item != undefined ? item.toString().replace('É', '').trim() : "";
    return (value.length > 0) ? value : "";
}

/**
 *
 * Pass all functions into module
 */
angular
    .module('inspinia')
    .controller('MainCtrl', MainCtrl)
    .controller('translateCtrl', translateCtrl)

    /**
    * Controller App
    */
    .controller('homeCtrl', homeCtrl)
    .controller('cargaCtrl', cargaCtrl)
    .controller('accesoCtrl', accesoCtrl)
    .controller('facturasCtrl', facturasCtrl)
    .controller('modalCtrl', modalCtrl)
