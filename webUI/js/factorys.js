﻿/**
 * LG - Exports
 *
 *
 * Functions (services)
 *  - accesoService
 *
 *
 */

/**
 * accesoService - service
 * xxxx
 *
 */
function accesoService($http, $cookieStore, $rootScope, CONSTANTES) {

    var service = {};

    service.accesoUser = function (user, password, callback, error)
    {
        var data = {
            usr: user,
            pwd: password
        };

        $http({
            url: CONSTANTES.RUTA_API + CONSTANTES.API_LOGIN,
            dataType: "json",
            method: "POST",
            data: data,
            headers: {
                'Accept': "application/json, charset=utf-8"
            }
        }).success(function (response) {
            callback(response);
        }).error(function (response) {
            error(response);
        });
    };

    service.setCredentials = function (user)
    {
        $rootScope.user = {
            Id: user.Id,
            Nombre: user.Nombre,
            Email: user.Email,
            Perfil: user.Perfil,
            Status: user.Status
        };
        $cookieStore.put('user', $rootScope.user);
    };

    service.clearCredentials = function ()
    {
        $rootScope.user = false;
        $cookieStore.remove('user');
    };

    return service;
}


function uploadFile($http, $q, $cookieStore, $rootScope, CONSTANTES) {

    var uploadFile = {};

    uploadFile.upload = function (file, contenedor, carpeta) {
        var referred = $q.defer();
        var fromData = new FormData();
        //console.log(contenedor);
        fromData.append("file", file);
        fromData.append("loadPlan", contenedor.LoadPlan);
        fromData.append("trailer", contenedor.Trailer);
        fromData.append("carpeta", carpeta);

        return $http.post(CONSTANTES.RUTA_API + CONSTANTES.API_UPLOAD_FILE, fromData, {
            headers: { 'Content-Type': undefined },
            transformRequest: angular.identity
            //data: contenedor
        }).success(function (res) {
            referred.resolve(res);
        }).error(function (msg, code) {
            referred.reject(res);
        });

        return referred.promise;
    }

    return uploadFile;
}


//filter Multiple...
function filterMultiple($filter) {
    return function (items, keyObj) {
        var filterObj = {
            data:items,
            filteredData:[],
            applyFilter : function(obj,key){
                var fData = [];
                if(this.filteredData.length == 0)
                    this.filteredData = this.data;
                if(obj){
                    var fObj = {};
                    if(angular.isString(obj)){
                        fObj[key] = obj;
                        fData = fData.concat($filter('filter')(this.filteredData,fObj));
                    }else if(angular.isArray(obj)){
                        if(obj.length > 0){	
                            for(var i=0;i<obj.length;i++){
                                if(angular.isString(obj[i])){
                                    fObj[key] = obj[i];
                                    fData = fData.concat($filter('filter')(this.filteredData,fObj));	
                                }
                            }
											
                        }										
                    }									
                    if(fData.length > 0){
                        this.filteredData = fData;
                    }
                }
            }
        };
    }
}


function unique () {
    //Filtro para hacer registros unicos en consulta de registros repetidos 
    //(en un ng-repeat) tipo DISTINT de sql
    return function (items, filterOn) {
        if (filterOn === false) {
            return items;
        }
        if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
            var hashCheck = {}, newItems = [];
            var extractValueToCompare = function (item) {
                if (angular.isObject(item) && angular.isString(filterOn)) {
                    return item[filterOn];
                } else {
                    return item;
                }
            };
            angular.forEach(items, function (item) {
                var valueToCheck, isDuplicate = false;
                for (var i = 0; i < newItems.length; i++) {
                    if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (!isDuplicate) {
                    newItems.push(item);
                }
            });
            items = newItems;
        }
        return items;
    };
}

/**
 *
 * Pass all functions into module
 */
angular
    .module('inspinia')
    .factory('accesoService', accesoService)
    .factory('uploadFile', uploadFile)
    .filter('filterMultiple', filterMultiple)
    .filter('unique', unique);

