﻿var misImagenes = {
    Lote: 'LoteA',
    Producto: 'ProductoA',
    Imagenes: []
};
$(document).ready(function () {
    abrirCamara();
    
    misImagenes.Lote = getParameterByName('qrsLote');
    misImagenes.Producto = getParameterByName('qrsProducto');

    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');
    var video = document.getElementById('video');

    document.getElementById("video").addEventListener("click", function () {
        context.drawImage(video, 0, 0, 640, 480);

        var jpegUrl = canvas.toDataURL("image/jpeg");

        var BASE64 = {
            Base64: jpegUrl,
            Producto: misImagenes.Producto,
            Lote: misImagenes.Lote
        };

        var miID = guardarFotoProducto(BASE64);
        misImagenes.Imagenes.push(miID);

        var image = new Image();
        image.src = jpegUrl;

        $('#misMinis').append('<div class="" data-id="' + miID.Resultado + '"><img data-id="' + miID.Resultado + '" src="' + jpegUrl + '" id="imgMini" class="mini" /></div>');
    });

    $("body").on('click', '.mini', function () {
        var objThis = this;
        $.each(misImagenes.Imagenes, function (indexReg, registro) {
            if ($(objThis).attr('data-id') == registro.Resultado) {
                //alert(registro.Mensaje);

                var image = new Image();
                image.src = $(objThis).attr('src');

                var w = window.open("");
                w.document.write(image.outerHTML);
            }
        });       
    });
});

function abrirCamara() {
    var video = document.getElementById('video');

    // Get access to the camera!
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        });
    }

    /* Legacy code below: getUserMedia 
    else if(navigator.getUserMedia) { // Standard
        navigator.getUserMedia({ video: true }, function(stream) {
            video.src = stream;
            video.play();
        }, errBack);
    } else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia({ video: true }, function(stream){
            video.src = window.webkitURL.createObjectURL(stream);
            video.play();
        }, errBack);
    } else if(navigator.mozGetUserMedia) { // Mozilla-prefixed
        navigator.mozGetUserMedia({ video: true }, function(stream){
            video.src = window.URL.createObjectURL(stream);
            video.play();
        }, errBack);
    }
    */
}

function guardarFotoProducto(BASE64) {
    var params = {
        Based64BinaryString: BASE64.Base64,
        Lote: BASE64.Lote,
        Producto: BASE64.Producto
    };
    var lstData = [];
    $.ajax({
        type: "POST",
        url: "../dataConnect.aspx/guardarImagenPrevio",
        async: false,
        data: JSON.stringify(params),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            lstData = data.d;
        },
        error: function (error) {
            alert(error.responseText);
        }
    });
    return lstData;
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}