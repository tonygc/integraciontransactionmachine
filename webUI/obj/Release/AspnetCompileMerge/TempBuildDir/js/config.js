/**
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, IdleProvider, KeepaliveProvider) {

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $urlRouterProvider.otherwise("/home/carga");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider
        .state('acceso', {
            url: '/acceso',
            controller: 'accesoCtrl',
            templateUrl: 'views/acceso/acceso.html',
            data: { pageTitle: 'Acceso' }
        })
        .state('home', {
            abstract: true,
            url: '/home',
            controller: 'homeCtrl',
            templateUrl: 'views/home/home.html',
            data: { pageTitle: 'Inicio' }
        })
        .state('home.carga', {
            url: '/carga',
            controller: 'cargaCtrl',
            templateUrl: 'views/carga/carga.html',
            data: { pageTitle: 'Cargar XLS' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        /* moment - manejo de fechas */
                        {
                            files: ['js/plugins/moment/moment.min.js', 'js/plugins/dotdotdot/jquery.dotdotdot.min.js']
                        },
                        {
                            files: ['js/xlsx/xlsx.full.min.js']
                        },
                        {
                            files: ['js/xlsx/xlsx.extendscript.js']
                        },
                        {
                            files: ['css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css']
                        },
                        {
                            files: ['css/plugins/iCheck/custom.css', 'js/plugins/iCheck/icheck.min.js']
                        },
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ngSweetAlert',
                            files: ['js/plugins/sweetalert-input/ngSweetAlert.js']
                        }
                    ]);
                }
            }
        })
        .state('home.facturas', {
            url: '/facturas',
            controller: 'facturasCtrl',
            templateUrl: 'views/facturas/generarfacturas.html',
            data: { pageTitle: 'Generar Facturas' },
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        /* moment - manejo de fechas */
                        {
                            files: ['js/plugins/moment/moment.min.js', 'js/plugins/dotdotdot/jquery.dotdotdot.min.js']
                        },

                        /* dataTables */
                        {
                            serie: true,
                            files: ['js/plugins/dataTables/datatables.min.js', 'css/plugins/dataTables/datatables.min.css']
                        },
                        {
                            serie: true,
                            name: 'datatables',
                            files: ['js/plugins/dataTables/angular-datatables.min.js']
                        },
                        {
                            serie: true,
                            name: 'datatables.buttons',
                            files: ['js/plugins/dataTables/angular-datatables.buttons.min.js']
                        },

                        /* Controles de formularios varios */
                        {
                            name: 'ui.knob',
                            files: ['js/plugins/jsKnob/jquery.knob.js', 'js/plugins/jsKnob/angular-knob.js']
                        },
                        {
                            files: ['css/plugins/ionRangeSlider/ion.rangeSlider.css', 'css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css', 'js/plugins/ionRangeSlider/ion.rangeSlider.min.js']
                        },
                        {
                            insertBefore: '#loadBefore',
                            name: 'localytics.directives',
                            files: ['css/plugins/chosen/bootstrap-chosen.css', 'js/plugins/chosen/chosen.jquery.js', 'js/plugins/chosen/chosen.js']
                        },
                        {
                            name: 'nouislider',
                            files: ['css/plugins/nouslider/jquery.nouislider.css', 'js/plugins/nouslider/jquery.nouislider.min.js', 'js/plugins/nouslider/angular-nouislider.js']
                        },
                        {
                            name: 'datePicker',
                            files: ['css/plugins/datapicker/angular-datapicker.css', 'js/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            files: ['js/plugins/jasny/jasny-bootstrap.min.js']
                        },
                        {
                            files: ['css/plugins/clockpicker/clockpicker.css', 'js/plugins/clockpicker/clockpicker.js']
                        },
                        {
                            name: 'ui.switchery',
                            files: ['css/plugins/switchery/switchery.css', 'js/plugins/switchery/switchery.js', 'js/plugins/switchery/ng-switchery.js']
                        },
                        {
                            name: 'colorpicker.module',
                            files: ['css/plugins/colorpicker/colorpicker.css', 'js/plugins/colorpicker/bootstrap-colorpicker-module.js']
                        },
                        {
                            name: 'ngImgCrop',
                            files: ['js/plugins/ngImgCrop/ng-img-crop.js', 'css/plugins/ngImgCrop/ng-img-crop.css']
                        },
                        {
                            serie: true,
                            files: ['js/plugins/daterangepicker/daterangepicker.js', 'css/plugins/daterangepicker/daterangepicker-bs3.css']
                        },
                        {
                            name: 'daterangepicker',
                            files: ['js/plugins/daterangepicker/angular-daterangepicker.js']
                        },
                        {
                            files: ['css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css']
                        },
                        {
                            name: 'ui.select',
                            files: ['js/plugins/ui-select/select.min.js', 'css/plugins/ui-select/select.min.css']
                        },
                        {
                            files: ['css/plugins/touchspin/jquery.bootstrap-touchspin.min.css', 'js/plugins/touchspin/jquery.bootstrap-touchspin.min.js']
                        },
                        {
                            name: 'ngTagsInput',
                            files: ['js/plugins/ngTags//ng-tags-input.min.js', 'css/plugins/ngTags/ng-tags-input-custom.min.css']
                        },
                        {
                            files: ['js/plugins/dualListbox/jquery.bootstrap-duallistbox.js', 'css/plugins/dualListbox/bootstrap-duallistbox.min.css']
                        },
                        {
                            name: 'frapontillo.bootstrap-duallistbox',
                            files: ['js/plugins/dualListbox/angular-bootstrap-duallistbox.js']
                        },

                        /* Libs para mensajes modal*/
                        {
                            files: ['js/plugins/sweetalert/sweetalert.min.js', 'css/plugins/sweetalert/sweetalert.css']
                        },
                        {
                            name: 'oitozero.ngSweetAlert',
                            files: ['js/plugins/sweetalert/angular-sweetalert.min.js']
                        },
                        {
                            name: 'ngSweetAlert',
                            files: ['js/plugins/sweetalert-input/ngSweetAlert.js']
                        },

                        {
                            files: ['css/plugins/iCheck/custom.css', 'js/plugins/iCheck/icheck.min.js']
                        }

                    ]);
                }
            }
        });
}

angular
    .module('inspinia')
    .config(config)
    .service('CONSTANTES', function ($location) {

        //this.RUTA_API = 'http://localhost:52322/';
        this.RUTA_API = 'http://45.61.48.157/apiIntegracionTM/';


        this.API_LOGIN = 'api/users/login';
        this.API_PAYMENTS_EMAIL = 'api/providers/paymentEmail';
        this.API_FACTURAS = 'api/payments/get?';
        this.API_FACTURAR = 'api/payments/standarPassive';

        this.URL_LOGIN_PAGE = 'http://localhost:52322/#/acceso';

        this.PERMISOS = [];
        this.PERMISOS["Administrador"] = ["CARGAR XLS"];

        this._languageESdataTable = {
            "decimal": "",
            "emptyTable": "Ning&uacute;n dato disponible para mostrar",
            "info": "_START_ al _END_ de _TOTAL_ registros",
            "infoEmpty": "0 registros",
            "infoFiltered": "(filtrados de un total de _MAX_ registros)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "_INPUT_",
            "searchPlaceholder": "Buscar...",
            "zeroRecords": "No se encontraron resultados",
            "paginate": {
                "first": "Primero",
                "last": "&Uacute;ltimo",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "aria": {
                "sortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        };

    })
    .run(function ($rootScope, $location, $cookieStore, $http, $window, $state, $stateParams, CONSTANTES) {

        $rootScope.$state = $state;
        $rootScope.user = $cookieStore.get('user') || false;
        $rootScope.$on('$stateChangeStart', function (event, state, params, fromState, fromParams) {
            //console.log("state->", state);
            if (!$rootScope.user) {
                $rootScope.user = false;
                $cookieStore.remove('user');
                $window.location.href = CONSTANTES.URL_LOGIN_PAGE;
                if (state.url.indexOf("carga") > -1 || state.url.indexOf("facturas") > -1) {
                    event.preventDefault();
                }
                //$location.path('/acceso');
            }
            else {
                if (state.url.indexOf("/home/facturas") > -1) {
                    $location.path('/home/facturas');
                } else {
                    $location.path('/home/carga');
                }
            }
        });

    });
