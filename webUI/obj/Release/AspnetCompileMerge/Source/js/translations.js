/**
 * INSPINIA - Responsive Admin Theme
 *
 */
function config($translateProvider) {

    $translateProvider
        .translations('en', {

            // Define all menu elements
            CARGA: 'Loading XLS',
            CANCELAR: 'Cancel',
            SENDEMAIL: 'Send Emails',
            SAVELAYOUT: 'Save Layout',
            LENGUAGE: 'Change language',
            CERRAR: 'Log out',
            CERRARWINDOW: 'Close',
            LAYOUT: 'Services',
            FACTURAS: 'Create invoices',
            FACTURAS_LIST: 'Invoices',
            FACTURAR: 'Billing',

            // Define some custom text
            WELCOME: 'Welcome ',
            MESSAGEINFO: 'Kiosko',
            SEARCH: 'Search',
            DEMO: 'Internationalization (sometimes shortened to \"I18N , meaning \"I - eighteen letters -N\") is the process of planning and implementing products and services so that they can easily be adapted to specific local languages and cultures, a process called localization . The internationalization process is sometimes called translation or localization enablement .',

            BTN_UPLOAD: 'Select the upload file...',

            MSJ_INF_CARGA_1: 'To continue, provide the file to load.',
            MSJ_INF_CARGA_2: 'To continue, provide the file to load.',

            MSJ_RES_CARGA: 'Verify that the information obtained is correct, otherwise correct the file and reload it.',

            //Sweet alert messages
            TITLE_ENVIAR_FACTURAS: 'Do you want to confirm this action?',
            TEXT_ENVIAR_FACTURAS: 'The invoices for the selected records will be generated.',
            CONFIRM_BUTTON_TEXT: 'Yes, confirm',
            CANCEL_BUTTON_TEXT: 'Cancel',
            ATENTION: 'Atention',
            INFORMATION: 'Information',
            SELECT_ONE_INVOICE: 'Select at least one invoice.',
            PROCESSED_INVOICES: " processed invoice(s).",
            PROCESSED_LAYOUT: " layout successfully processed.",
            SELECT_ALL: 'Select all',
            ELEMENTS: ' elements found',
            SENT: 'Sent',
            OF: 'of',
            TOTAL_RECORDS: 'Total of records:',
            BILLING: 'Billing',
            INCORRECT_INVOICES_TEXT: 'Incorrect invoices',
            SUCCESSFULL_INVOICES_TEXT: 'Invoices successfully sent',
            LIST_OF_ERRORS: 'List of errors',
            ELEMENTS_SENT: 'Elements sent',
            UPLOAD_COMPLETE: 'Upload complete!',
            UPLOAD_COMPLETE_DESC: 'Some inconsistencies were found, fix the file and upload it again.',
            NOT_ALLOWED_FILE: 'File not allowed!',
            NOT_ALLOWED_FILE_DESC: 'Only Excel files are allowed.',
            INVALID_COLUMNS: 'Columns are no valid!',
            INVALID_COLUMNS_DESC1: 'Only excel files with columns and valid format are allowed.',
            EMPTY_FILE: 'Empty file!',
            EMPTY_FILE_DESC: 'Records were not found',
            HEADER_ERRORS: 'File error!',
            HEADER_ERRORS_DESC: 'Invalid header columns were found. Verify number, order and column naming.'
        })
        .translations('es', {

            // Elementos de menu
            CARGA: 'Cargar XLS',
            CANCELAR: 'Cancelar',
            SENDEMAIL: 'Enviar Correos',
            SAVELAYOUT: 'Guardar Layout',
            LENGUAGE: 'Cambiar lenguage',
            CERRAR: 'Cerrar sesión',
            CERRARWINDOW: 'Cerrar',
            LAYOUT: 'Servicios',
            FACTURAR: 'Facturar',
            FACTURAS: 'Generar Facturas',
            FACTURAS_LIST: 'Facturas',

            // Define some custom text
            WELCOME: 'Bienvenido ',
            MESSAGEINFO: 'Kiosko',
            SEARCH: 'Buscar',
            DEMO: 'Internacionalización (a veces abreviado como \"I18N, que significa\" I - dieciocho letras N \") es el proceso de planificación e implementación de productos y servicios de manera que se pueden adaptar fácilmente a las lenguas y culturas locales específicas, un proceso llamado localización El proceso de internacionalización. a veces se llama la traducción o la habilitación de localización.',

            BTN_UPLOAD: 'Seleccione el archivo de carga...',

            MSJ_INF_CARGA_1: 'Para continuar proporcione el archivo a cargar.',
            MSJ_INF_CARGA_2: 'Para continuar proporcione el archivo a cargar.',

            MSJ_RES_CARGA: 'Verifique que la información obtenida sea correcta, en caso contrario corrija el archivo y vuelva a cargarlo.',

            //Sweet alert messages
            TITLE_ENVIAR_FACTURAS: '¿Seguro de realizar la acción?',
            TEXT_ENVIAR_FACTURAS: 'Se facturarán los registros seleccionados.',
            TEXT_GUARDAR_LAYOUT: 'Se enviará correo a los proveedores seleccionados.',
            CONFIRM_BUTTON_TEXT: 'Sí, realizar',
            CANCEL_BUTTON_TEXT: 'Cancelar',
            ATENTION: 'Atención',
            INFORMATION: 'Información',
            SELECT_ONE_INVOICE: 'Seleccione al menos una factura.',
            PROCESSED_INVOICES: " factura(s) procesada(s).",
            PROCESSED_LAYOUT: " layout procesado exitosamente.",
            SELECT_ALL: 'Seleccionar todos',
            ELEMENTS: ' elementos obtenidos',
            SENT: 'Enviados',
            OF: 'de',
            TOTAL_RECORDS: 'Total de registros:',
            BILLING: 'Facturación',
            INCORRECT_INVOICES_TEXT: 'Facturas incorrectas',
            SUCCESSFULL_INVOICES_TEXT: 'Facturas enviadas correctamente',
            LIST_OF_ERRORS: 'Listado de errores',
            ELEMENTS_SENT: 'Elementos enviados',
            UPLOAD_COMPLETE: 'Carga completa!',
            UPLOAD_COMPLETE_DESC: 'Se encontraron incongruencias de datos, corrija el archivo y vuelva a cargarlo.',
            NOT_ALLOWED_FILE: 'Archivo no permitido!',
            NOT_ALLOWED_FILE_DESC: 'Sólo se permiten archivos de Excel.',
            INVALID_COLUMNS: 'Las columnas del archivo no son válidas!',
            INVALID_COLUMNS_DESC1: 'Solo se permiten archivos de Excel con información y formato válido.',
            EMPTY_FILE: 'Archivo vacío!',
            EMPTY_FILE_DESC: 'No se encontraron datos. Verifique que el archivo contenga datos para carga.',
            HEADER_ERRORS: 'Error en archivo!',
            HEADER_ERRORS_DESC: 'Encabezados de columnas incorrectos. Verifique el número, order y nombrado de columnas del archivo para cargar los datos.'
        });

    $translateProvider.preferredLanguage('es');

}

angular
    .module('inspinia')
    .config(config);