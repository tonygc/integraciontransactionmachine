/**
 * INSPINIA - Responsive Admin Theme
 *
 * Main directives.js file
 * Define directives for used plugin
 *
 *
 * Functions (directives)
 *  - sideNavigation
 *  - iboxTools
 *  - minimalizaSidebar
 *  - vectorMap
 *  - sparkline
 *  - icheck
 *  - ionRangeSlider
 *  - dropZone
 *  - responsiveVideo
 *  - chatSlimScroll
 *  - customValid
 *  - fullScroll
 *  - closeOffCanvas
 *  - clockPicker
 *  - landingScrollspy
 *  - fitHeight
 *  - iboxToolsFullScreen
 *  - slimScroll
 *  - truncate
 *  - touchSpin
 *  - markdownEditor
 *  - resizeable
 *  - bootstrapTagsinput
 *  - passwordMeter
 *
 */


/**
 * pageTitle - Directive for set Page title - mata title
 */
function pageTitle($rootScope, $timeout) {
    return {
        link: function(scope, element) {
            var listener = function(event, toState, toParams, fromState, fromParams) {
                // Default title - load on Dashboard 1
                var title = 'Inicio';
                // Create your own title pattern
                if (toState.data && toState.data.pageTitle) title = 'KIOSKO | ' + toState.data.pageTitle;
                $timeout(function() {
                    element.text(title);
                });
            };
            $rootScope.$on('$stateChangeStart', listener);
        }
    }
};

/**
 * sideNavigation - Directive for run metsiMenu on sidebar navigation
 */
function sideNavigation($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element) {
            // Call the metsiMenu plugin and plug it to sidebar navigation
            $timeout(function(){
                element.metisMenu();
            });

            // Colapse menu in mobile mode after click on element
            var menuElement = $('#side-menu a:not([href$="\\#"])');
            menuElement.click(function(){
                if ($(window).width() < 769) {
                    $("body").toggleClass("mini-navbar");
                }
            });

            // Enable initial fixed sidebar
            //if ($("body").hasClass('fixed-sidebar')) {
            //    var sidebar = element.parent();
            //    sidebar.slimScroll({
            //        height: '100%',
            //        railOpacity: 0.9,
            //    });
            //}
        }
    };
};

/**
 * responsibleVideo - Directive for responsive video
 */
function responsiveVideo() {
    return {
        restrict: 'A',
        link:  function(scope, element) {
            var figure = element;
            var video = element.children();
            video
                .attr('data-aspectRatio', video.height() / video.width())
                .removeAttr('height')
                .removeAttr('width');

            //We can use $watch on $window.innerWidth also.
            $(window).resize(function() {
                var newWidth = figure.width();
                video
                    .width(newWidth)
                    .height(newWidth * video.attr('data-aspectRatio'));
            }).resize();
        }
    }
}

/**
 * iboxTools - Directive for iBox tools elements in right corner of ibox
 */
function iboxTools($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.children('.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };
                // Function for close ibox
                $scope.closebox = function () {
                    var ibox = $element.closest('div.ibox');
                    ibox.remove();
                }
        }
    };
}


function iboxToolsShowHide($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools_showhide.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.children('.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };
            // Function for full screen
            $scope.fullscreen = function () {
                var ibox = $element.closest('div.ibox');
                var button = $element.find('i.fa-expand');
                if (button.length == 0) button = $element.find('i.fa-compress');

                $('body').toggleClass('fullscreen-ibox-mode');
                button.toggleClass('fa-expand').toggleClass('fa-compress');                
                ibox.toggleClass('fullscreen');
                setTimeout(function () {
                    $(window).trigger('resize');
                }, 100);
            }
        }
    };
}

/**
 * iboxTools with full screen - Directive for iBox tools elements in right corner of ibox with full screen option
 */
function iboxToolsFullScreen($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/ibox_tools_full_screen.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var ibox = $element.closest('div.ibox');
                var icon = $element.find('i:first');
                var content = ibox.children('.ibox-content');
                content.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                ibox.toggleClass('').toggleClass('border-bottom');
                $timeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            };
            // Function for close ibox
            $scope.closebox = function () {
                var ibox = $element.closest('div.ibox');
                ibox.remove();
            };
            // Function for full screen
            $scope.fullscreen = function () {
                var ibox = $element.closest('div.ibox');
                var button = $element.find('i.fa-expand');
                $('body').toggleClass('fullscreen-ibox-mode');
                button.toggleClass('fa-expand').toggleClass('fa-compress');
                ibox.toggleClass('fullscreen');
                setTimeout(function() {
                    $(window).trigger('resize');
                }, 100);
            }
        }
    };
}

/**
 * minimalizaSidebar - Directive for minimalize sidebar
*/
function minimalizaSidebar($timeout) {
    return {
        restrict: 'A',
        template: '<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="" ng-click="minimalize()"><i class="fa fa-bars"></i></a>',
        controller: function ($scope, $element) {
            $scope.minimalize = function () {
                $("body").toggleClass("mini-navbar");
                if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
                    // Hide menu in order to smoothly turn on when maximize menu
                    $('#side-menu').hide();
                    // For smoothly turn on menu
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(400);
                        }, 200);
                } else if ($('body').hasClass('fixed-sidebar')){
                    $('#side-menu').hide();
                    setTimeout(
                        function () {
                            $('#side-menu').fadeIn(400);
                        }, 100);
                } else {
                    // Remove all inline style from jquery fadeIn function to reset menu state
                    $('#side-menu').removeAttr('style');
                }
            }
        }
    };
};


function closeOffCanvas() {
    return {
        restrict: 'A',
        template: '<a class="close-canvas-menu" ng-click="closeOffCanvas()"><i class="fa fa-times"></i></a>',
        controller: function ($scope, $element) {
            $scope.closeOffCanvas = function () {
                $("body").toggleClass("mini-navbar");
            }
        }
    };
}

/**
 * vectorMap - Directive for Vector map plugin
 */
function vectorMap() {
    return {
        restrict: 'A',
        scope: {
            myMapData: '=',
        },
        link: function (scope, element, attrs) {
            var map = element.vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 0.9,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    }
                },
                series: {
                    regions: [
                        {
                            values: scope.myMapData,
                            scale: ["#1ab394", "#22d6b1"],
                            normalizeFunction: 'polynomial'
                        }
                    ]
                },
            });
            var destroyMap = function(){
                element.remove();
            };
            scope.$on('$destroy', function() {
                destroyMap();
            });
        }
    }
}


/**
 * sparkline - Directive for Sparkline chart
 */
function sparkline() {
    return {
        restrict: 'A',
        scope: {
            sparkData: '=',
            sparkOptions: '=',
        },
        link: function (scope, element, attrs) {
            scope.$watch(scope.sparkData, function () {
                render();
            });
            scope.$watch(scope.sparkOptions, function(){
                render();
            });
            var render = function () {
                $(element).sparkline(scope.sparkData, scope.sparkOptions);
            };
        }
    }
};

/**
 * icheck - Directive for custom checkbox icheck
 */
function icheck($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, element, $attrs, ngModel) {
            return $timeout(function() {
                var value;
                value = $attrs['value'];

                $scope.$watch($attrs['ngModel'], function(newValue){
                    $(element).iCheck('update');
                })

                return $(element).iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'

                }).on('ifChanged', function(event) {
                        if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                            $scope.$apply(function() {
                                return ngModel.$setViewValue(event.target.checked);
                            });
                        }
                        if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                            return $scope.$apply(function() {
                                return ngModel.$setViewValue(value);
                            });
                        }
                    });
            });
        }
    };
}

/**
 * ionRangeSlider - Directive for Ion Range Slider
 */
function ionRangeSlider() {
    return {
        restrict: 'A',
        scope: {
            rangeOptions: '='
        },
        link: function (scope, elem, attrs) {
            elem.ionRangeSlider(scope.rangeOptions);
        }
    }
}

/**
 * dropZone - Directive for Drag and drop zone file upload plugin
 */
function dropZone() {
    return {
        restrict: 'C',
        link: function(scope, element, attrs) {

            var config = {
                url: 'http://localhost:8080/upload',
                maxFilesize: 100,
                paramName: "uploadfile",
                maxThumbnailFilesize: 10,
                parallelUploads: 1,
                autoProcessQueue: false
            };

            var eventHandlers = {
                'addedfile': function(file) {
                    scope.file = file;
                    if (this.files[1]!=null) {
                        this.removeFile(this.files[0]);
                    }
                    scope.$apply(function() {
                        scope.fileAdded = true;
                    });
                },

                'success': function (file, response) {
                }

            };

            dropzone = new Dropzone(element[0], config);

            angular.forEach(eventHandlers, function(handler, event) {
                dropzone.on(event, handler);
            });

            scope.processDropzone = function() {
                dropzone.processQueue();
            };

            scope.resetDropzone = function() {
                dropzone.removeAllFiles();
            }
        }
    }
}

/**
 * chatSlimScroll - Directive for slim scroll for small chat
 */
//function chatSlimScroll($timeout) {
//    return {
//        restrict: 'A',
//        link: function(scope, element) {
//            $timeout(function(){
//                element.slimscroll({
//                    height: '234px',
//                    railOpacity: 0.4
//                });

//            });
//        }
//    };
//}

/**
 * customValid - Directive for custom validation example
 */
function customValid(){
    return {
        require: 'ngModel',
        link: function(scope, ele, attrs, c) {
            scope.$watch(attrs.ngModel, function() {

                // You can call a $http method here
                // Or create custom validation

                var validText = "Inspinia";

                if(scope.extras == validText) {
                    c.$setValidity('cvalid', true);
                } else {
                    c.$setValidity('cvalid', false);
                }

            });
        }
    }
}


/**
 * fullScroll - Directive for slimScroll with 100%
 */
//function fullScroll($timeout){
//    return {
//        restrict: 'A',
//        link: function(scope, element) {
//            $timeout(function(){
//                element.slimscroll({
//                    height: '100%',
//                    railOpacity: 0.9
//                });

//            });
//        }
//    };
//}

/**
 * slimScroll - Directive for slimScroll with custom height
 */
//function slimScroll($timeout){
//    return {
//        restrict: 'A',
//        scope: {
//            boxHeight: '@'
//        },
//        link: function(scope, element) {
//            $timeout(function(){
//                element.slimscroll({
//                    height: scope.boxHeight,
//                    railOpacity: 0.9
//                });

//            });
//        }
//    };
//}

/**
 * clockPicker - Directive for clock picker plugin
 */
function clockPicker() {
    return {
        restrict: 'A',
        link: function(scope, element) {
                element.clockpicker();
        }
    };
};


/**
 * landingScrollspy - Directive for scrollspy in landing page
 */
function landingScrollspy(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.scrollspy({
                target: '.navbar-fixed-top',
                offset: 80
            });
        }
    }
}

/**
 * fitHeight - Directive for set height fit to window height
 */
function fitHeight(){
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.css("height", $(window).height() + "px");
            element.css("min-height", $(window).height() + "px");
        }
    };
}

/**
 * truncate - Directive for truncate string
 */
function truncate($timeout){
    return {
        restrict: 'A',
        scope: {
            truncateOptions: '='
        },
        link: function(scope, element) {
            $timeout(function(){
                element.dotdotdot(scope.truncateOptions);

            });
        }
    };
}


/**
 * touchSpin - Directive for Bootstrap TouchSpin
 */
function touchSpin() {
    return {
        restrict: 'A',
        scope: {
            spinOptions: '='
        },
        link: function (scope, element, attrs) {
            scope.$watch(scope.spinOptions, function(){
                render();
            });
            var render = function () {
                $(element).TouchSpin(scope.spinOptions);
            };
        }
    }
};

/**
 * markdownEditor - Directive for Bootstrap Markdown
 */
function markdownEditor() {
    return {
        restrict: "A",
        require:  'ngModel',
        link:     function (scope, element, attrs, ngModel) {
            $(element).markdown({
                savable:false,
                onChange: function(e){
                    ngModel.$setViewValue(e.getContent());
                }
            });
        }
    }
};


/**
 * passwordMeter - Directive for jQuery Password Strength Meter
 */
function passwordMeter() {
    return {
        restrict: 'A',
        scope: {
            pwOptions: '='
        },
        link: function (scope, element, attrs) {
            scope.$watch(scope.pwOptions, function(){
                render();
            });
            var render = function () {
                $(element).pwstrength(scope.pwOptions);
            };
        }
    }
};


function fileread() {
    
    function link(scope, element, attrs) {
        var X = XLSX;

        function validarDato(dato, i) {
            var _dato = { error: false, dato: dato };
            if (dato) {
                dato = dato.trim();
                dato.replace(" ", "");
                _dato.dato = dato;
                if (dato === "") {
                    _dato.error = true;
                }
                if (i == 1) {
                    if (dato.length != 10) {
                        _dato.error = true;
                    }
                }
                if (i == 3) {
                    if (dato.length != 14) {
                        _dato.error = true;
                    }
                }
                if (i == 5) {
                    if (dato.length == 10) {
                        var date = new Date(dato);
                        if (!date instanceof Date && isNaN(date.valueOf())) {
                            _dato.error = true;
                        }
                    }
                    else _dato.error = true;
                }
                if (i == 6) {
                    if (dato.length >= 5 && dato.length <= 8) {
                        //_dato.dato = hora;
                    }
                    else _dato.error = true;
                }
            } else {
                _dato.error = true;
            }

            return _dato;
        };

        function validarEncabezados(key) {
            if (key.length != 7) return false;
            var defaul = ["TYPE", "TRAILER", "RECINTO", "LOAD_PLAN", "DESTINATION", "GATE_OUT_DATE", "GATE_OUT_TIME"];
            key.forEach(function (element, index, array) {
                if (element != defaul[index]) return false;
            });
            return true;
        };

        function json_wb(result) {
            console.log("json_wb");
            console.log(result);
            var error, key = [], obj = [], arr = [];
            key = result[0];
            result.forEach(function (element, index, array) {
                if (index != 0) {
                    obj = [];
                    error = false;
                    for (var i = 0; i < key.length; i++) {
                        var dato = validarDato(element[i], i);
                        obj[key[i]] = dato.dato;
                        //if (dato.error && !error) error = true;
                        if (dato.error) obj["ERROR_" + key[i]] = true;
                    }
                    //if (error) obj["ERROR"] = true;
                    arr.push(obj);
                }
            });
            scope.$parent.cargarArchivo(arr);
        };

        function to_json(data) {
            var workbook = X.read(data, { type: 'array' });
            var result = {}, hojas = 0;
            workbook.SheetNames.forEach(function (sheetName) {
                if (hojas == 0) {
                    var roa = X.utils.sheet_to_json(workbook.Sheets[sheetName], { header: 1 });
                    if (roa.length > 1) {
                        result = roa;
                        if (validarEncabezados(result[0])) {
                            console.log("result");
                            console.log(result);
                            json_wb(result);
                        }
                        else scope.$parent.errorEncabezados = true;
                    }
                    else scope.$parent.archivoVacio = true;
                }
                hojas++;
            });
        }

        element.bind("change", function (changeEvent) {
            var file = changeEvent.target.files[0];
            if (file) {
                var array = [".csv", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.openxmlformats-officedocument.spreadsheetml.template"];
                var reader = new FileReader();
                if (array.indexOf(file.type) != -1) {
                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            var data = loadEvent.target.result;
                            data = new Uint8Array(data);
                            to_json(data);
                        });
                    }
                }
                else {
                    scope.$parent.archivoNoPermitido = true;
                }
                reader.readAsArrayBuffer(file);
            }
        });
    }

    return {
        scope: {
            fileread: "="
        },
        link: link
    }
};


function uploadxls() {
    function link(scope, element, attrs) {       
        var X = XLSX;
        
        function validarDato(dato, i) {
            var _dato = { error: false, dato: dato };
            if (dato) {
                dato = dato.trim();
                dato.replace(" ", "");
                _dato.dato = dato;
                if (dato === "") {
                    _dato.error = true;
                }
                if (i == 1) {
                    if (dato.length != 10) {
                        _dato.error = true;
                    }
                }
                if (i == 3) {
                    if (dato.length != 14) {
                        _dato.error = true;
                    }
                }
                if (i == 5) {
                    if (dato.length == 10) {
                        var date = new Date(dato);
                        if (!date instanceof Date && isNaN(date.valueOf())) {
                            _dato.error = true;
                        }
                    }
                    else _dato.error = true;
                }
                if (i == 6) {
                    if (dato.length >= 5 && dato.length <= 8) {
                    }
                    else _dato.error = true;
                }
            } else {
                _dato.error = true;
            }
            
            return _dato;
        };

        function validarEncabezados(key) {
            if (key.length != 7) return false;
            var defaul = ["TYPE","TRAILER","RECINTO","LOAD_PLAN","DESTINATION","GATE_OUT_DATE","GATE_OUT_TIME"];
            key.forEach(function (element, index, array) {
                if (element != defaul[index]) return false;
            });
            return true;
        };

        function identificarFilaKeys(result) {
            var columnas = ['ARCHIVO_DE_ORIGEN_DE_TRANSACCI', 'ESTADO_DE_COINCIDENCIA', 'COINCIDENCIA_EN', 'ING_FECHA', 'PRO_CODIGO', 'PRO_NOMBRE', 'CI_CODIGO', 'CRI_DESCRIPCION', 'FOLTDA_CODIGO', 'FOLIOCC', 'ING_SINCONTRIBUCION', 'ING_IMPORTE', 'ING_CARGO', 'ING_CONTRIBUCION', 'ING_REFERENCIA', 'DIS_CONTABLE', 'CENTROCOSTO', 'REGIONCONTABLE', 'ZONACONTABLE'];
            var indexKeys = null;
            var conteo = 0;
            var BreakException = {};

            try {
                result.forEach(function (element, index, array) {
                    for (var i = 0; i < columnas.length; i++) {
                        if (element.indexOf(columnas[i]) != -1) {
                            conteo++;
                        }
                    }
                    //if (conteo == columnas.length) {
                    if (conteo > 0) {
                        indexKeys = index;
                        throw BreakException;
                    }
                });
            } catch (e) {
                //if (e !== BreakException) throw e;
                //console.log('Exception: ' + e)
                //console.log('BreakException: ' + BreakException)
            }
            return indexKeys;
        };

        function json_wb(result, filaKeys) {
            try {
                var error, key = [], obj = [], arr = [];
                key = result[filaKeys];


                result.forEach(function (element, index, array) {
                    
                    if (element[4] == "003992") {
                        console.log("element");
                        console.log(element);
                    }
                    if (index > filaKeys) {
                        obj = [];
                        error = false;
                        for (var i = 0; i < key.length; i++) {
                            //var dato = validarDato(element[i], i);
                            obj[key[i]] = element[i];
                            //if (dato.error) obj["ERROR_"+key[i]] = true;
                        }
                        arr.push(obj);
                    }
                });

                scope.$parent.cargarXLS(arr);
                scope.$parent.setKeys(key);
            }
            catch (e) {

                scope.$parent.archivoNoPermitido = true;
            
            }
           
        };

        function to_json(data) {
            var workbook = X.read(data, { type: 'array' });
            var result = {}, hojas = 0;
            workbook.SheetNames.forEach(function (sheetName) {                
                if (hojas === 0) {
                    var roa = X.utils.sheet_to_json(workbook.Sheets[sheetName], { header: 1 });
                    const jsonData = X.utils.sheet_to_json(workbook.Sheets[sheetName]);
                    //X.writeFileSync(
                    //    workbook,
                    //    "testData.json",
                    //    JSON.stringify(jsonData, null, 4)
                    //);
                    if (roa.length > 1) {
                        result = roa;

                        var filaKeys = identificarFilaKeys(result);
                        
                        json_wb(result, filaKeys);

                    }
                    else scope.$parent.archivoVacio = true;
                }
                hojas++;
            });
        }

        element.bind("change", function (changeEvent) {

            var palabra = ".xlsx";
            var texto = "";
            var resultado;

            var file = changeEvent.target.files[0];
            try {
                if (file.name == null) {
                    texto = "archivo";
                }
                else {
                    texto = file.name;
                }
            }
            catch (e)
            {
                scope.$parent.archivoNoPermitido = true;
            }
            
            resultado = texto.indexOf(palabra);

            if (resultado != -1) {
                if (file) {
                    if (resultado != -1) {
                        scope.$parent.setFile(file);
                        scope.$parent.loading = true;
                        scope.$parent.$apply();
                        var array = [".csv", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.openxmlformats-officedocument.spreadsheetml.template"];
                        var reader = new FileReader();
                        //if (array.indexOf(file.type) != -1) {
                            reader.onload = function (loadEvent) {
                                scope.$apply(function () {
                                    var data = loadEvent.target.result;
                                    data = new Uint8Array(data);
                                    console.log("data");
                                    console.log(data);
                                    to_json(data);
                                });
                            };
                        //}
                        //else {
                        //    console.log("not valid?");
                        //    scope.$parent.noValidos(false, true, false, false, false);
                        //}
                        scope.$parent.loading = false;
                        reader.readAsArrayBuffer(file);
                    }
                    else {
                        scope.$parent.loading = false;
                        scope.$parent.noValidos(false, true, false, false, false);
                    }

                }
            }
            else
            {
                scope.$parent.loading = false;
                scope.$parent.noValidos(false,true,false,false,false);
            }
           
        });
    }

    return {
        scope: {
            fileread: "="
        },
        link: link
    }
};


function uploadFiles($parse, CONSTANTES) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on("change", function (e) {
                $parse(attrs.uploadFiles).assign(scope, element[0].files[0]);
            });
        }
    }
};

function editableTd() {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.css("cursor", "pointer");
            element.attr('contenteditable', 'true');
            if (attrs.type == "number") {
                element.keypress(function (event) {
                    if (attrs.type == "number" && event.keyCode < 48 || event.keyCode > 57)
                        return false;
                });
            }

            //lement.bind('blur keyup change', function () {
            element.bind('change', function () {
                scope.marcas[attrs.row][attrs.field] = element.text();
                scope.$apply();
            });

            element.bind('click', function () {
                document.execCommand('selectAll', false, null)
            });
        }
    }
};


/**
 *
 * Pass all functions into module
 */
angular
    .module('inspinia')
    .directive('pageTitle', pageTitle)
    .directive('sideNavigation', sideNavigation)
    .directive('iboxTools', iboxTools)
    .directive('minimalizaSidebar', minimalizaSidebar)
    .directive('vectorMap', vectorMap)
    .directive('sparkline', sparkline)
    .directive('icheck', icheck)
    .directive('ionRangeSlider', ionRangeSlider)
    .directive('dropZone', dropZone)
    .directive('responsiveVideo', responsiveVideo)
    //.directive('chatSlimScroll', chatSlimScroll)
    .directive('customValid', customValid)
    //.directive('fullScroll', fullScroll)
    .directive('closeOffCanvas', closeOffCanvas)
    .directive('clockPicker', clockPicker)
    .directive('landingScrollspy', landingScrollspy)
    .directive('fitHeight', fitHeight)
    .directive('iboxToolsFullScreen', iboxToolsFullScreen)
    //.directive('slimScroll', slimScroll)
    .directive('truncate', truncate)
    .directive('touchSpin', touchSpin)
    .directive('markdownEditor', markdownEditor)
    .directive('passwordMeter', passwordMeter)


    //Directives de la APP
    .directive('fileread', fileread)
    .directive('editableTd', editableTd)
    .directive('uploadxls', uploadxls)
    .directive('iboxToolsShowHide', iboxToolsShowHide)
    .directive('uploadFiles', uploadFiles);


