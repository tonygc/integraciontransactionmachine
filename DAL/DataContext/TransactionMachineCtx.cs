﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.ViewModels;

namespace DAL.DataContext
{
    public class TransactionMachineCtx : DbContext
    {
        public TransactionMachineCtx() : base("transactionmachine_db")
        {
        }

        public TransactionMachineCtx(string connection) : base(connection)
        {
        }

        public DbSet<TBL_ParametrosAuxiliares> TBL_ParametrosAuxiliares { get; set; }
        public DbSet<TBL_Pagos> TBL_Pagos { get; set; }
    }
}
