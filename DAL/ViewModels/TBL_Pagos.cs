﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ViewModels
{
    public class TBL_Pagos
    {
        [Key]
        public long IdPago { get; set; }
        public string ARCHIVO_DE_ORIGEN_DE_TRANSACCI { get; set; }
        public string ESTADO_DE_COINCIDENCIA { get; set; }
        public DateTime COINCIDENCIA_EN { get; set; }
        public DateTime ING_FECHA { get; set; }
        public string PRO_CODIGO { get; set; }
        public string PRO_NOMBRE { get; set; }
        public string CI_CODIGO { get; set; }
        public string CRI_DESCRIPCION { get; set; }
        public string FOLTDA_CODIGO { get; set; }
        [Index("IX_FOLIOCC", 1, IsUnique = true)]
        public string FOLIOCC { get; set; }
        public decimal ING_SINCONTRIBUCION { get; set; }
        public decimal ING_IMPORTE { get; set; }
        public decimal ING_CARGO { get; set; }
        public decimal ING_CONTRIBUCION { get; set; }
        public string ING_REFERENCIA { get; set; }
        public string DIS_CONTABLE { get; set; }
        public string CENTROCOSTO { get; set; }
        public string REGIONCONTABLE { get; set; }
        public string ZONACONTABLE { get; set; }
        public string InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime? FechaFactura { get; set; }
    }
}
