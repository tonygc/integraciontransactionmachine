﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ViewModels
{
    public class TBL_ParametrosAuxiliares
    {
        [Key]
        public int IdParametro { get; set; }
        public string Descripcion { get; set; }
        public string Valor { get; set; }
        public string Valor2 { get; set; }
        public string Grupo { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaModificacion { get; set; }
    }
}
