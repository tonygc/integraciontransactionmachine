﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using BussinessLayer.Interfaces.Definition;
using BussinessLayer.Interfaces.Implementation;
using BussinessLayer.OutputResponses;
using RestSharp;
using RestSharp.Authenticators;

namespace UtileriasIntegracionTM.QuanamKiosko
{
    public class Providers
    {
        public static async Task<ProvidersQuanamResponse> GetByProId(string proid)
        {
            ProvidersQuanamResponse response = new ProvidersQuanamResponse();
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                ITBL_ParametrosAuxiliares interfaz = new TBL_ParametrosAuxiliaresIMP();
                string urlBase = (await interfaz.getById(1)).Data.FirstOrDefault()?.Valor;
                string methodGetDataProvider = (await interfaz.getById(4)).Data.FirstOrDefault()?.Valor;
                string urlParameters = $"?No_Proveedor={proid}";

                var client = new RestClient(urlBase);
                string userName = (await interfaz.getById(2)).Data.FirstOrDefault()?.Valor;
                string password = (await interfaz.getById(3)).Data.FirstOrDefault()?.Valor;
                client.Authenticator = new HttpBasicAuthenticator(userName, password);
                var request = new RestRequest($"{methodGetDataProvider}{urlParameters}", Method.GET);
                var queryResult = client.Execute(request);
                if(queryResult.IsSuccessful)
                {
                    response = Newtonsoft.Json.JsonConvert.DeserializeObject<ProvidersQuanamResponse>(queryResult.Content);
                }
                else
                {
                    response.Response = TypeResponse.ERROR;
                    response.Message = queryResult.Content;
                }
                //HttpClient client = new HttpClient();
                //client.BaseAddress = new Uri($"{urlBase}{methodGetDataProvider}");

                //// Add an Accept header for JSON format.
                //client.DefaultRequestHeaders.Accept.Add(
                //new MediaTypeWithQualityHeaderValue("application/json"));

                //string yourusername = (await interfaz.getById(2)).Data.FirstOrDefault()?.Valor;
                //string yourpwd = (await interfaz.getById(3)).Data.FirstOrDefault()?.Valor;

                //string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(yourusername + ":" + yourpwd));
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", encoded);

                //// List data response.
                //HttpResponseMessage responsehttp = await client.GetAsync(urlParameters);  // Blocking call! Program will wait here until a response is received or a timeout occurs.

                //HttpWebRequest httpWebRequest = WebRequest.Create($"{urlBase}{methodGetDataProvider}?No_Proveedor={proid}") as HttpWebRequest;
                //httpWebRequest.ContentType = "application/json";
                //httpWebRequest.Method = WebRequestMethods.Http.Get;

                //string yourusername = (await interfaz.getById(2)).Data.FirstOrDefault()?.Valor;
                //string yourpwd = (await interfaz.getById(3)).Data.FirstOrDefault()?.Valor;

                //string encoded = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(yourusername + ":" + yourpwd));
                //httpWebRequest.Headers.Add("Authorization", "Basic " + encoded);
                //httpWebRequest.Expect = "application/json";

                //HttpWebResponse responsehttp = httpWebRequest.GetResponse() as HttpWebResponse;
                //Encoding EncodingResp = Encoding.GetEncoding(responsehttp.CharacterSet);

                //string responses = "";
                //using (System.IO.Stream responseStream = responsehttp.GetResponseStream())
                //{
                //    using (var reader = new System.IO.StreamReader(responseStream, EncodingResp))
                //    {
                //        responses = reader.ReadToEnd();
                //    }
                //}

                
            }
            catch (Exception ex)
            {
                response.Response = TypeResponse.ERROR;
                response.Message = $"{ex.Message} {ex.InnerException?.Message}";
                response.StackTrace = $"{ex.StackTrace}";
            }
            return response;
        }
    }
}
