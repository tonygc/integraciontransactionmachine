﻿using BussinessLayer.Interfaces.Definition;
using BussinessLayer.Interfaces.Implementation;
using BussinessLayer.OutputResponses;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BussinessLayer.InputRequests;

namespace UtileriasIntegracionTM.QuanamKiosko
{
    public class Payments
    {
        public static async Task<InvoiceInterfaceResponseINT> GeneratePassive(InvoiceInterfaceHeaderRequest o)
        {
            InvoiceInterfaceResponseINT response = new InvoiceInterfaceResponseINT();
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                ITBL_ParametrosAuxiliares interfaz = new TBL_ParametrosAuxiliaresIMP();
                string urlBase = (await interfaz.getById(1)).Data.FirstOrDefault()?.Valor;
                string methodGetDataProvider = (await interfaz.getById(5)).Data.FirstOrDefault()?.Valor;
                //string urlParameters = $"?No_Proveedor={proid}";

                var client = new RestClient(urlBase);
                string userName = (await interfaz.getById(2)).Data.FirstOrDefault()?.Valor;
                string password = (await interfaz.getById(3)).Data.FirstOrDefault()?.Valor;
                client.Authenticator = new HttpBasicAuthenticator(userName, password);

                
                var request = new RestRequest($"{methodGetDataProvider}", Method.POST);

                string jsonToSend = Newtonsoft.Json.JsonConvert.SerializeObject(o);
                request.AddParameter("application/json; charset=utf-8", jsonToSend, ParameterType.RequestBody);
                request.RequestFormat = DataFormat.Json;

                var queryResult = client.Execute(request);
                if (queryResult.IsSuccessful)
                {
                    response = Newtonsoft.Json.JsonConvert.DeserializeObject<InvoiceInterfaceResponseINT>(queryResult.Content);
                }
                else
                {
                    response.Response = TypeResponse.ERROR;
                    response.Message = queryResult.Content;
                }
            }
            catch (Exception ex)
            {
                response.Response = TypeResponse.ERROR;
                response.Message = $"{ex.Message} {ex.InnerException?.Message}";
                response.StackTrace = $"{ex.StackTrace}";
            }
            return response;
        }
    }
}
