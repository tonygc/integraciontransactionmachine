﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using BussinessLayer.OutputResponses;

namespace UtileriasIntegracionTM
{
    public class AttatchementEmail
    {
        /// <summary>
        /// Stream from file
        /// </summary>
        public Stream Binary { get; set; }
        /// <summary>
        /// Content Type from file. example new ContentType("application/vnd.ms-excel")
        /// </summary>
        public ContentType Type { get; set; }
        /// <summary>
        /// Name from file attatchment with extension example "file.xls"
        /// </summary>
        public string Name { get; set; }
    }
    public class Email
    {
        public static GenericResponse SendBySTMP(string addressSMTP, string sender, string password, IEnumerable<string> recipients,
            string subject, string body, IEnumerable<AttatchementEmail> attachments, int port, bool ssl, bool isHtml = false)
        {
            GenericResponse response = new GenericResponse();

            try
            {
                using (var smtpMail = new SmtpClient(addressSMTP))
                {
                    using (var email = new MailMessage())
                    {
                        email.Sender = new MailAddress(sender.Trim());
                        email.From = new MailAddress(sender.Trim());
                        email.Subject = subject;
                        email.Body = body;
                        //recipients.ToList().RemoveAll(a => !a.Contains("ing.josean"));
                        foreach (var recipient in recipients)
                        {
                            email.To.Add(new MailAddress(recipient.Trim()));
                        }
                        foreach (var fileSend in attachments)
                        {
                            //new ContentType("application/vnd.ms-excel")
                            var attachment = new Attachment(fileSend.Binary, fileSend.Type);
                            attachment.Name = fileSend.Name;//"PagoDeServicios.xls"
                            attachment.ContentDisposition.Size = fileSend.Binary.Length;
                            email.Attachments.Add(attachment);
                        }
                        email.IsBodyHtml = isHtml;
                        smtpMail.UseDefaultCredentials = false;
                        smtpMail.Port = port;
                        smtpMail.EnableSsl = ssl;
                        smtpMail.Credentials = new NetworkCredential(sender, password);
                        smtpMail.Send(email);
                    }
                    //email.Attachments.Dispose();
                }
            }
            catch (Exception ex)
            {
                response.Response = TypeResponse.ERROR;
                response.Message = $"{ex.Message} {ex.InnerException?.Message}";
            }
            return response;
        }
    }
}
